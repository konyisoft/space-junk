﻿using System.Linq;
using UnityEngine;

namespace Konyisoft
{
	public class AspectRatio : Singleton<AspectRatio>
	{
		#region Fields and properties

		public bool autoupdateCameras = true;

		Camera[] cameras;
		float screenWidth;
		float screenHeight;

		#endregion

		#region Constans

		const float ScreenX = 16.0f;
		const float ScreenY = 9.0f;

		#endregion

		#region Mono methods

		void Start()
		{
			screenWidth = Screen.width;
			screenHeight = Screen.height;
		}

		void Update()
		{
			// Updates automatically if screen size changed
			if (autoupdateCameras && (screenWidth != Screen.width || screenHeight != Screen.height))
			{
				SetCamerasAspect(cameras);
				screenWidth = Screen.width;
				screenHeight = Screen.height;
			}
		}

		#endregion

		#region Public methods

		public void AddCamera(params Camera[] cameras)
		{
			this.cameras = new Camera[0];
			this.cameras = cameras;
			SetCamerasAspect(this.cameras);
		}

		#endregion

		#region Private methods

		float GetTargetAspectRatio()
		{
			// The desired aspect ratio
			return ScreenX / ScreenY;
		}

		float GetWindowAspectRatio()
		{
			// The game window's current aspect ratio
			return (float)Screen.width / (float)Screen.height;
		}

		float GetScaleHeight()
		{
			// Current viewport height should be scaled by this amount
			return GetWindowAspectRatio() / GetTargetAspectRatio();
		}

		void SetCamerasAspect(params Camera[] cameras)
		{
			if (cameras == null)
				return;

			cameras.ToList().ForEach(cam =>
			{
				if (cam != null)
				{
					float scaleHeight = GetScaleHeight();

					// if scaled height is less than current height, add letterbox
					if (scaleHeight < 1.0f)
					{
						Rect rect = cam.rect;

						rect.width = 1.0f;
						rect.height = scaleHeight;
						rect.x = 0;
						rect.y = (1.0f - scaleHeight) / 2.0f;

						cam.rect = rect;
					}
					else // add pillarbox
					{
						float scaleWidth = 1.0f / scaleHeight;

						Rect rect = cam.rect;

						rect.width = scaleWidth;
						rect.height = 1.0f;
						rect.x = (1.0f - scaleWidth) / 2.0f;
						rect.y = 0;

						cam.rect = rect;
					}
				}
			});
		}

		#endregion
	}
}