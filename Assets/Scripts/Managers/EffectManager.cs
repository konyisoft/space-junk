using UnityEngine;

namespace Konyisoft
{
	public class EffectManager : Singleton<EffectManager>
	{
		#region Fields and properties

		#endregion
		
		#region Mono methods

		#endregion
		
		#region Public methods

		public GameObject InstantiateEffect(string prefabName, Vector3 position)
		{
			GameObject go = Instantiate(ResourceManager.Instance.GetPrefab("Effects/" + prefabName));
			if (go != null)
			{
				go.transform.position = position;
				TemporaryManager.Instance.Add(go);
			}
			return go;
		}
		
		public void InstantiateRipples(Vector3 position)
		{
			InstantiateEffect("Ripples", position);
		}

		public void InstantiateExplosion(Vector3 position)
		{
			InstantiateEffect("Explosion", position);
		}

		public void InstantiateLightExplosion(Vector3 position)
		{
			InstantiateEffect("Light Explosion", position);
		}

		public void InstantiateWhiteSmoke(Vector3 position)
		{
			InstantiateEffect("White Smoke", position);
		}
	
		public void ShakeCurrentCamera()
		{
			Camera currentCamera = DisplayManager.Instance.CurrentCamera;
			if (currentCamera != null)
			{
				Shaker shaker = currentCamera.GetComponent<Shaker>();
				if (shaker != null)
					shaker.Shake();
			}
		}
		
		#endregion
		
		#region Private methods
		
		#endregion
	}
}