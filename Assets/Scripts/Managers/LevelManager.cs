using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using SimpleJSON;

namespace Konyisoft
{
	public class LevelManager : Singleton<LevelManager>
	{
		#region Fields and properties

		public int LevelCount
		{
			get { return levels.Count; }
		}

		public bool LevelWasSet
		{
			get { return currentIndex > -1; }
		}

		public int CurrentIndex
		{
			get { return currentIndex; }
		}

		public Brick[,,] CurrentBricks
		{
			get { return currentBricks; }
		}

		public LevelData CurrentData
		{
			get { return currentData; }
		}

		public TextAsset dataFile;
		public Vector3 brickStartPosition = Vector3.zero;
		public Vector3 brickSize = Vector3.zero;

		List<LevelData> levels = new List<LevelData>();
		int currentIndex = -1;
		Brick[,,] currentBricks = new Brick[COLS, ROWS, FLOORS];
		LevelData currentData = new LevelData();
		bool requestFallCheck = false;
		GameObject additionalObject;

		// Constant aliases
		const int ROWS = Constants.Level.Rows;
		const int COLS = Constants.Level.Cols;
		const int FLOORS = Constants.Level.Floors;
		const char EMPTY = Constants.Level.Empty;
		const char HOLDER = Constants.Level.Holder;

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();

			if (dataFile == null)
				Debug.LogError("No data file attached.");
			else if (!Parse())
				Debug.LogError("Level parse error.");
		}

		void Update()
		{
			if (requestFallCheck)
			{
				FallCheck();
				requestFallCheck = false;
			}
		}

		#endregion

		#region Public methods

		public void DestroyCurrentLevel()
		{
			if (LevelWasSet)
			{
				for (int floor = 0; floor < FLOORS; floor++)
				{
					for (int col = 0; col < COLS; col++)
					{
						for (int row = 0; row < ROWS; row++)
						{
							if (currentBricks[col, row, floor] != null)
							{
								Destroy(currentBricks[col, row, floor].gameObject);
								currentBricks[col, row, floor] = null;
							}
						}
					}
				}

				if (additionalObject != null)
					Destroy(additionalObject);

				currentIndex = -1;
				currentData.Reset();
			}
		}

		public void SetLevel(int index)
		{
			DestroyCurrentLevel();
			if (index >= 0 && index < LevelCount)
			{
				InstantiateLevel(index);
				currentIndex = index;
				RequestFallCheck();
			}
			else
				Debug.LogError("Level index out of range.");
		}

		public void RemoveBrick(Brick brick)
		{
			if (LevelWasSet && brick != null && !IsSpaceCharacter(currentData.structure[brick.Col, brick.Row, brick.Floor]))
			{
				currentData.structure[brick.Col, brick.Row, brick.Floor] = EMPTY;
				currentData.brickCount--;
				currentData.destructibleBrickCount--;
				Destroy(brick.gameObject);
				RequestFallCheck();
			}
		}

		public List<Brick> GetNeighboursOf(Brick brick)
		{
			List<Brick> neighbours = new List<Brick>();

			if (LevelWasSet && brick != null)
			{
				// NOTE: Check the structure here!
				// A brick destroys in the next Update() so the currentBricks element can be
				// not null at the moment of destroying. It is safer to check the structure
				// because it shows the current state at the moment. See: GameManager DetonateBrick()
				int col = brick.Col;
				int row = brick.Row;
				int floor = brick.Floor;

				// Left
				if (col > 0 && !IsSpaceCharacter(currentData.structure[col - 1, row, floor]))
					neighbours.Add(currentBricks[col - 1, row, floor]);

				// Right
				if (col < COLS - 1 && !IsSpaceCharacter(currentData.structure[col + 1, row, floor]))
					neighbours.Add(currentBricks[col + 1, row, floor]);

				// Back
				if (row > 0 && !IsSpaceCharacter(currentData.structure[col, row - 1, floor]))
					neighbours.Add(currentBricks[col, row - 1, floor]);

				// Front
				if (row < ROWS - 1 && !IsSpaceCharacter(currentData.structure[col, row + 1, floor]))
					neighbours.Add(currentBricks[col, row + 1, floor]);

				// Down (NOTE: maybe not needed in the game)
				if (floor > 0 && !IsSpaceCharacter(currentData.structure[col, row, floor - 1]))
					neighbours.Add(currentBricks[col, row, floor - 1]);

				// Top
				if (floor < FLOORS - 1 && !IsSpaceCharacter(currentData.structure[col, row, floor + 1]))
					neighbours.Add(currentBricks[col, row, floor + 1]);

				// Remove possible nulls
				neighbours.RemoveAll(item => item == null);
			}
			return neighbours;
		}

		public void RequestFallCheck()
		{
			requestFallCheck = true;
		}

		public bool HasPrefab(char c)
		{
			return Constants.Level.brickPrefabs.ContainsKey(c);
		}

		#endregion

		#region Private methods

		bool Parse()
		{
			// Parsing JSON, get root node
			JSONNode rootNode = JSON.Parse(dataFile.text);

			if (rootNode == null)
			{
				Debug.LogError("JSON parse error.");
				return false;
			}

			// Get levels node
			JSONNode levelsNode = rootNode["levels"];

			if (levelsNode == null)
			{
				Debug.LogError("No 'levels' node found in JSON data file.");
				return false;
			}

			// Iterate through levels
			for (int i = 0; i < levelsNode.Count; i++)
			{
				// Create levelData object
				LevelData levelData = new LevelData();
				levelData.id = levelsNode[i]["id"];
				levelData.powerUpChance = levelsNode[i]["powerUpChance"].AsInt;
				levelData.background = levelsNode[i]["background"];
				levelData.additionalPrefab = levelsNode[i]["additionalPrefab"];

				// Get structure
				JSONNode structureNode = levelsNode[i]["structure"];
				// Not found
				if (structureNode == null)
				{
					Debug.LogError("No structure node found in level #" + i);
					return false;
				}

				// Rows count doesn't equal
				if (structureNode.Count != ROWS)
				{
					Debug.LogError("Not exactly " + ROWS + " rows in level #" + i);
					return false;
				}

				// Iterate through structure (rows)
				for (int row = 0; row < structureNode.Count; row++)
				{
					// Split row to floors
					string rowString = structureNode[row];
					List<string> floors = rowString.Split('|').ToList();

					if (floors.Count != FLOORS)
					{
						Debug.LogError("Not exactly " + FLOORS + " floors in level #" + i);
						return false;
					}

					// Separate floors to characters
					for (int floor = 0; floor < floors.Count; floor++)
					{
						string line = floors[floor];

						if (line.Length != COLS)
						{
							Debug.LogError("Not exactly " + COLS + " characters a line in level #" + i);
							return false;
						}

						for (int col = 0; col < line.Length; col++)
						{
							char ch = line[col];

							// Has any prefab related to character?
							if (HasPrefab(ch))
							{
								levelData.structure[col, row, floor] = ch;
								levelData.brickCount++;
							}
							// Holder
							else if (ch == HOLDER)
							{
								levelData.structure[col, row, floor] = HOLDER;
							}
							// Unknown character
							else
							{
								levelData.structure[col, row, floor] = EMPTY;
							}
						}
					}
				}
				levels.Add(levelData);
			}
			return true;
		}

		void InstantiateLevel(int index)
		{
			if (index >= 0 && index < LevelCount)
			{
				Vector3 position = brickStartPosition;
				currentData.CopyFrom(levels[index]); // Copy level data

				for (int floor = 0; floor < FLOORS; floor++)
				{
					for (int col = 0; col < COLS; col++)
					{
						for (int row = 0; row < ROWS; row++)
						{
							char c = currentData.structure[col, row, floor];

							if (Constants.Level.brickPrefabs.ContainsKey(c))
							{
								// Instantiating object
								GameObject prefab = ResourceManager.Instance.GetPrefab("Bricks/" + Constants.Level.brickPrefabs[c]);  // Get prefab by name

								if (prefab != null)
								{
									GameObject brickObject = Instantiate(prefab);
									Brick brick = brickObject.GetComponent<Brick>();
									brickObject.transform.position = position;
									TemporaryManager.Instance.Add(brickObject);
									brick.SetCoords(col, row, floor);
									currentBricks[col, row, floor] = brick;

									// Count destructible ones only
									if (!brick.indestructible)
										currentData.destructibleBrickCount++;
								}
							}
							currentData.structure[col, row, floor] = c;
							position.z -= brickSize.z;
						}
						position.x += brickSize.x;
						position.z = brickStartPosition.z;
					}
					position.x = brickStartPosition.x;
					position.y += brickSize.y;
					position.z = brickStartPosition.z;
				}

				// Trying to instantiate the additional prefab of the level
				if (!string.IsNullOrEmpty(currentData.additionalPrefab))
				{
					GameObject additionalPrefab = ResourceManager.Instance.GetPrefab("Additional/" + currentData.additionalPrefab);
					if (additionalPrefab != null)
					{
						additionalObject = Instantiate(additionalPrefab);
						TemporaryManager.Instance.Add(additionalObject);
					}
				}
			}
			else
			{
				Debug.LogError("No level found with index #" + index);
			}
		}

		// Don't call it directly, use RequestFallCheck() instead. It waits for the next Update().
		void FallCheck()
		{
			if (LevelWasSet)
			{
				// Checkig from down to top.
				for (int floor = 1; floor < FLOORS; floor++)  // No need to check the lowest floor
				{
					for (int col = 0; col < COLS; col++)
					{
						for (int row = 0; row < ROWS; row++)
						{
							char currentChar = currentData.structure[col, row, floor];
							// Is it a brick?
							if (Constants.Level.brickPrefabs.ContainsKey(currentChar))
							{
								// Check all floors below the current brick and fall while empty spaces found
								int lowerFloor = floor - 1;
								char lowerChar = currentData.structure[col, row, lowerFloor];

								while (lowerFloor >= 0 && lowerChar == EMPTY)
								{
									// Change characters in structure array
									currentData.structure[col, row, lowerFloor + 1] = EMPTY;
									currentData.structure[col, row, lowerFloor] = currentChar;

									Brick currentBrick = currentBricks[col, row, lowerFloor + 1];
									// Change object references in instance array
									if (currentBrick != null)
									{
										currentBricks[col, row, lowerFloor + 1] = null;
										currentBricks[col, row, lowerFloor] = currentBrick;
										currentBrick.SetCoords(col, row, lowerFloor);  // Change coords
										currentBrick.FallDown(); // Fall down a height of 1 brick
									}

									lowerFloor--;
									if (lowerFloor < 0)
										break;
									else
										lowerChar = currentData.structure[col, row, lowerFloor];

								}  // while
							}  // if
						}  // for (row)
					}  // for (col)
				}  // for (floor)
			}
		}
		
		bool IsSpaceCharacter(char c)
		{
			// True if c is holder or empty character
			return c == HOLDER || c == EMPTY;
		}

		#endregion
	}
}