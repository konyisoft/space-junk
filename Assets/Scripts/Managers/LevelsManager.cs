using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using SimpleJSON;

namespace Konyisoft
{
	public class LevelsManager : Singleton<LevelsManager>
	{
		#region Fields and proprties

		public Camera mainCamera;
		public Camera starfieldCamera;
		public Camera backgroundCamera;
		public TextAsset dataFile;
		public CanvasRenderer levelsPanel;
		public Color unlockedColor = Color.white;
		public Color lockedColor = Color.red;

		List<string> levelIDs = new List<string>();
		
		#endregion
		
		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			AspectRatio.Instance.AddCamera(mainCamera, starfieldCamera, backgroundCamera);
			Screen.sleepTimeout = SleepTimeout.NeverSleep;
			
			Globals.LoadPlayerPrefs();
			
			if (dataFile == null)
				Debug.LogError("No data file attached.");
			else if (!Parse())
				Debug.LogError("Level parse error.");
		}
		
		void Start()
		{
			InstantiateList();  // Important to be here in Start
			ScreenFader.Instance.FadeIn();
		}
		
		void Update()
		{
			// Screen fader 
			if (ScreenFader.Instance.IsFading)
				return;

			// Common input
			if (Input.GetButtonUp("Cancel") || Input.GetKeyUp(KeyCode.Escape))
				LoadTitle();
				
			// Desktop input
			#if UNITY_EDITOR || UNITY_STANDALONE
				DesktopInput();
			#endif
		}

		#endregion
		
		#region Public methods

		public void LoadTitle()
		{
			ScreenFader.Instance.FadeOut(OnLoadTitle);
		}
		
		#endregion
		
		#region Private methods
		
		void DesktopInput()
		{
			// Nothing to do at the moment
		}
		
		bool Parse()
		{
			levelIDs.Clear();
			
			// Parsing JSON, get root node
			JSONNode rootNode = JSON.Parse(dataFile.text);

			if (rootNode == null)
			{
				Debug.LogError("JSON parse error.");
				return false;
			}

			// Get levels node
			JSONNode levelsNode = rootNode["levels"];

			if (levelsNode == null)
			{
				Debug.LogError("No 'levels' node found in JSON data file.");
				return false;
			}

			// Iterate through levels and get their ids
			for (int i = 0; i < levelsNode.Count; i++)
			{
				levelIDs.Add(levelsNode[i]["id"]);
			}
			return true;
		}

		void InstantiateList()
		{
			if (levelsPanel == null)
				return;

			for (int i = 0; i < levelIDs.Count; i++)
			{
				GameObject prefab = ResourceManager.Instance.GetPrefab("UI/LevelRow");
				if (prefab != null)
				{
					GameObject levelRow = Instantiate(prefab);
					levelRow.transform.SetParent(levelsPanel.transform, false);

					string id = levelIDs[i];
					bool unlocked = Unlocked.IsLevelUnlocked(id);
				
					Text levelText = levelRow.transform.Find("LevelText").GetComponent<Text>();
					Button stateButton = levelRow.transform.Find("StateButton").GetComponent<Button>();
					Text stateText = levelRow.transform.Find("StateButton/Text").GetComponent<Text>();
					
					levelText.text = string.Format(Constants.Texts.LevelNum, i + 1);

					if (unlocked || i == 0)
					{
						stateText.text = Constants.Texts.LevelPlay;
						stateText.color = unlockedColor;
					}
					else
					{
						stateText.text = Constants.Texts.LevelLocked;
						stateText.color = lockedColor;
					}
					
					int index = i;
					stateButton.onClick.AddListener(delegate { OnClick_StateButton(id, index); });
				}
			}
			
			Canvas.ForceUpdateCanvases();
		}

		#endregion
		
		#region Event handlers
		
		void OnLoadTitle()
		{
			SceneManager.LoadScene(Constants.Scenes.Title);
		}
		
		void OnClick_StateButton(string id, int index)
		{
			if (Unlocked.IsLevelUnlocked(id) || index == 0)
			{
				AudioManager.Instance.PlaySound("Click");
				Globals.startLevelIndex = index;
				ScreenFader.Instance.FadeOut(OnStartGame);
			}
			else
			{
				AudioManager.Instance.PlaySound("Denied");
				// Nothing more to do here
			}
		}
		
		void OnStartGame()
		{
			SceneManager.LoadScene(Constants.Scenes.Game);
		}		

		#endregion
	}
}
