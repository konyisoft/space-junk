using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Konyisoft
{
	public class GameManager : Singleton<GameManager>
	{
		#region Enumerations

		public enum GameState
		{
			None,
			Initialize,
			WaitForLaunch,
			Playing,
			PlayingAndWaitForLaunch,   // When ball is stuck on paddle
			WaitForTryAgain,
			GameOver,
			LevelDone,
			WonTheGame
		}

		#endregion

		#region Fields and properties

		public GameState State
		{
			get { return state; }
		}

		public bool HasPaddle
		{
			get { return paddle != null; }
		}

		public bool HasBall
		{
			get { return balls.Count > 0; }
		}

		public Ball MainBall
		{
			get { return HasBall ? balls[0] : null; }
		}

		public int BallCount
		{
			get { return balls.Count; }
		}

		public Paddle paddle;
		public float scoreCounterSpeed = 0.01f;
		public float touchSensitivity = 0.8f;

		bool CanClickOrTapOnPaddle
		{
			get
			{
				return
					state == GameState.WaitForTryAgain ||
					state == GameState.WaitForLaunch ||
					state == GameState.PlayingAndWaitForLaunch ||
					state == GameState.LevelDone;
			}
		}

		List<Ball> balls = new List<Ball>();
		int score = 0;
		int lives = 0;
		int level = 0;
		int targetScore = 0;
		float scoreTimer = 0f;
		GameState state = GameState.None;
		int fingerId = -1;
		bool startSoundPlayed = false;

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			Globals.LoadPlayerPrefs();
			AudioManager.Instance.PreLoad();
		}
		
		void Start()
		{
			ScreenFader.Instance.FadeIn();
			RestartGame();
		}

		void Update()
		{
			if (GUIManager.Instance.QuitPanelVisible)
				return;
		
			// Counting score
			if (targetScore != score)
			{
				scoreTimer += Time.deltaTime;
				if (scoreTimer >= scoreCounterSpeed)
				{
					if (score < targetScore)
					{
						score += 100;
						if (score > targetScore)
							score = targetScore;
					}
					else
					{
						score -= 100;
						if (score < targetScore)
							score = targetScore;
					}
					GUIManager.Instance.WriteScore(score);
					scoreTimer = 0.0f;
				}
			}

			// Screen fader or end sequence
			if (ScreenFader.Instance.IsFading || State == GameState.WonTheGame)
				return;

			// Common input
			if (Input.GetButtonUp("Cancel") || Input.GetKeyUp(KeyCode.Escape))
				GUIManager.Instance.ShowQuitPanel();

			// Desktop input
#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBGL
				DesktopInput();
#endif

			// Mobile input
#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IPHONE)
				MobileInput();
#endif
		}

		#endregion

		#region Public methods

		public void StartLevel()
		{
			SetState(GameState.Initialize);

			UpdateGUI();
			GUIManager.Instance.ShowLargeText(string.Format(Constants.Texts.Level, level + 1), true);

			if (!LevelManager.Instance.LevelWasSet)
			{
				LevelManager.Instance.SetLevel(level);
				DisplayManager.Instance.background.LoadTexture("Backgrounds/" + LevelManager.Instance.CurrentData.background);
				Unlocked.AddLevel(LevelManager.Instance.CurrentData.id);
				LevelManager.Instance.RequestFallCheck();
			}

			if (!HasBall)
				CreateMainBall(Vector3.zero);

			if (HasPaddle)
			{
				paddle.NormalSize();
				paddle.NormalControl();
				paddle.AttachBall(MainBall, true);
				paddle.SetText(Constants.Texts.Launch);
				paddle.SetTextVisible(true);
				paddle.Play();
				GUIManager.Instance.TurnOffAllLeds();
			}

			if (!startSoundPlayed || Options.soundSetting == Options.SoundSetting.SoundOnly)
			{
				AudioManager.Instance.PlaySound("Ready Signal Loop");
				startSoundPlayed = true;
			}
			AudioManager.Instance.PlaySound("Ready Signal Ping");
			
			SetState(GameState.WaitForLaunch);
		}

		public void RestartGame()
		{
			ResetValues();
			
			if (level == 0 && !Globals.helpIconsShown)
			{
				GUIManager.Instance.ShowHelpIcons();
			}
			Globals.helpIconsShown = true;
			
			StartLevel();
		}

		public void NextLevel()
		{
			if (level < LevelManager.Instance.LevelCount - 1)
			{
				level++;
				StartLevel();
			}
			// This is for test. In real game it will never be executed.
			else
			{
				LevelDone();
			}
		}

		public void DestroyBrick(Brick brick)
		{
			// Instantiate power if the brick drops one
			// NOTE: only bricks on the lowest floor drop powerups (bombs can destroy the higher floors)
			if (brick.HasPowerUp && brick.Floor == 0 && LevelManager.Instance.CurrentData.destructibleBrickCount > 1)
			{
				// Global chance to drop powerup
				if (brick.alwaysDropPowerUp ||
					Random.Range(0, 100) <= LevelManager.Instance.CurrentData.powerUpChance)
				{
					PowerUpManager.PowerUpType powerUp = brick.DropPowerUp();

					if (powerUp != PowerUpManager.PowerUpType.Unset)
						PowerUpManager.Instance.InstantiatePowerUp(powerUp, brick.Transform.position);
				}
			}

			// Some special effects
			EffectManager.Instance.InstantiateExplosion(brick.Transform.position);
			EffectManager.Instance.ShakeCurrentCamera();

			LevelManager.Instance.RemoveBrick(brick);

			IncreaseScore(brick.score);	 // Score counting will be handled in Update

			if (LevelManager.Instance.CurrentData.destructibleBrickCount == 0)
			{
				LevelDone();
			}
		}

		public void DetonateBrick(Brick brick)
		{
			// Destfroy the bricks nearby (left, right, forward, badck and top)
			List<Brick> neighbours = LevelManager.Instance.GetNeighboursOf(brick);
			neighbours.ForEach(neighbour =>
			{
				// Additional score when destroying with a bomb
				if (!neighbour.indestructible)
					IncreaseScore(Constants.Game.BombAdditionalScore);

				neighbour.ForceDestroy(); // Important: causes recursion on bombs!
			});
		}

		public void GetPowerUp(PowerUp powerUp)
		{
			// Apply (with duration) and activate
			if (powerUp.applyable)
				PowerUpManager.Instance.ApplyEffect(powerUp.type);
			// Activate immediately
			else
				PowerUpManager.Instance.ActivateEffect(powerUp.type);

			AudioManager.Instance.PlaySound("PowerUp Get");
			GUIManager.Instance.InvokeFloatingText(Constants.Texts.PowerUps[powerUp.type]);
			EffectManager.Instance.InstantiateLightExplosion(powerUp.Transform.position);
			Destroy(powerUp.gameObject);
		}

		public void BallOutOfField(Ball ball)
		{
			RemoveBall(ball);

			// Has any more balls? Continue the game.
			if (HasBall)
			{
				// Make the 0th ball in list as main ball
				MainBall.startDirection = Ball.Direction.Forward;
			}
			// No more: die
			else
			{
				// SoundFX
				AudioManager.Instance.PlaySound("Ball is Out");
				// Stop game, destroy all powerups, etc...
				PowerUpManager.Instance.ClearAppliedEffects();
				PowerUpManager.Instance.EvaporateAll();
				GUIManager.Instance.TurnOffAllLeds();
				// Lives
				lives--;
				GUIManager.Instance.WriteLives(lives);
				// Cleanup
				TemporaryManager.Instance.Cleanup();

				// What to do next?
				if (lives <= 0)
					GameOver();
				else
					WaitForTryAgain();
			}
		}

		public void WaitForTryAgain()
		{
			SetState(GameState.WaitForTryAgain);
			GUIManager.Instance.ShowLargeText(Constants.Texts.Failed);
			paddle.NormalSize();
			paddle.NormalControl();
			paddle.SetText(Constants.Texts.TryAgain);
			paddle.SetTextVisible(true);
		}

		public void LevelDone()
		{
			SetState(GameState.LevelDone);

			// Fly away and destroy currently existing balls
			if (paddle.HasBallAttached)
				paddle.DetachBall();
			balls.ForEach(ball => ball.FlyAway());
		
			PowerUpManager.Instance.ClearAppliedEffects();
			PowerUpManager.Instance.EvaporateAll();
			GUIManager.Instance.TurnOffAllLeds();
			TemporaryManager.Instance.Cleanup();
			NormalBallSpeed();
			paddle.NormalSize();
			paddle.NormalControl();

			// Proceed to the next level
			if (level < LevelManager.Instance.LevelCount - 1)
			{
				paddle.SetText(Constants.Texts.Continue);
				paddle.SetTextVisible(true);
				// Get a random text for level done state
				string levelDoneText = Constants.Texts.LevelDone[Random.Range(0, Constants.Texts.LevelDone.Count)];
				GUIManager.Instance.ShowLargeText(levelDoneText);
			}
			// Won the game
			else
			{
				WonTheGame(); // WonTheGame state from here
			}
		}

		public void GameOver()
		{
			SetState(GameState.GameOver);
			AudioManager.Instance.Stop("Music", 0.4f);
			paddle.NormalSize();
			paddle.NormalControl();
			paddle.Stop();
			Globals.gameOverMode = GameOverManager.Mode.GameOver;
			ScreenFader.Instance.FadeOut(OnLoadGameOver, 4.25f); // Long fade out
		}

		public void WonTheGame()
		{
			SetState(GameState.WonTheGame);
			AudioManager.Instance.Stop("Music", 0.4f);
			paddle.SetText(string.Empty);
			paddle.SetTextVisible(false);
			paddle.Stop();
			Globals.gameOverMode = GameOverManager.Mode.WonTheGame;
			EndSequenceManager.Instance.PlaySequence();
		}
		
		public void EndSequenceEnded()
		{
			ScreenFader.Instance.FadeOut(OnLoadGameOver, 5f); // Long fade out
		}

		public void StickBall(Ball ball)
		{
			ball.Stop();
			paddle.AttachBall(ball, false);
			paddle.SetText(Constants.Texts.Launch);
			paddle.SetTextVisible(true);
			SetState(GameState.PlayingAndWaitForLaunch);
		}

		public void LoadTitle(bool immediate = false)
		{
			Cleanup();
			Globals.titleMode = TitleManager.Mode.Title;
			if (immediate)
			{
				OnLoadTitle();
			}
			else
			{
				AudioManager.Instance.Stop("Music", 0.3f);
				ScreenFader.Instance.FadeOut(OnLoadTitle);
			}
		}
		
		// Cheat!
		public void SkipLevel()
		{
			Cleanup();
			NextLevel();
		}

		#endregion

		#region Internal methods

		internal void AddBall(Ball ball)
		{
			if (ball != null)
				balls.Add(ball);
		}

		internal void RemoveBall(Ball ball)
		{
			if (balls.Contains(ball))
				balls.Remove(ball);
		}

		internal void ClearBalls()
		{
			balls.RemoveAll(ball => ball == null);	// Remove nulls (safety)
			balls.ForEach(ball => ball.Terminate());
			balls.Clear();
		}

		internal bool IsMainBall(Ball ball)
		{
			return ball == MainBall;
		}

		internal bool CanBallStick(Ball ball)
		{
			// Is StickingBall effect currently applied?
			return
				PowerUpManager.Instance.IsEffectApplied(PowerUpManager.PowerUpType.StickingBall) &&
				ball.IsPlaying &&
				state == GameState.Playing &&
				state != GameState.PlayingAndWaitForLaunch;
		}

		internal void SpeedUpBall()
		{
			balls.ForEach(ball => ball.SpeedUp());
		}

		internal void SpeedDownBall()
		{
			balls.ForEach(ball => ball.SpeedDown());
		}

		internal void NormalBallSpeed()
		{
			balls.ForEach(ball => ball.NormalSpeed());
		}

		internal void IncreaseLife(int value)
		{
			lives += 1;
			UpdateGUI();
		}

		internal void IncreaseScore(int value)
		{
			targetScore += value;
			Globals.lastScore += value;  // Increase global score immediately
			scoreTimer = 0.0f;
		}

		internal void ForceClickOrTapOnPaddle()
		{
			HandleClickOrTapOnPaddle();
		}
		
		internal Ball GetClosestBall(Vector3 position)
		{
			Ball closest = null;
			float lastDistance = Mathf.Infinity;
			balls.ForEach(ball => 
			{
				if (ball != null)
				{
					float distance = (ball.Transform.position - position).sqrMagnitude;
					if (closest == null || distance < lastDistance)
					{
						lastDistance = distance;
						closest = ball;
					}
				}
			});
			return closest;
		}
		
		internal void Cleanup()
		{
			ClearBalls();
			LevelManager.Instance.DestroyCurrentLevel();
			TemporaryManager.Instance.Clear();	// Destroys powerups, additional balls etc.
			fingerId = -1;
		}
		
		#endregion

		#region Private methods

		void ResetValues()
		{
			level = Globals.startLevelIndex;
			score = 0;
			lives = Constants.Game.MaxLives;
			scoreTimer = 0f;
			targetScore = score;
			fingerId = -1;
			
			Globals.lastScore = 0;
		}

		void UpdateGUI()
		{
			GUIManager.Instance.WriteScore(score);
			GUIManager.Instance.WriteLives(lives);
		}

		void SetState(GameState state)
		{
			this.state = state;
		}

		GameObject CreateMainBall(Vector3 position)
		{
			GameObject go = Instantiate(ResourceManager.Instance.GetPrefab("Ball/MainBall"));

			if (go != null)
			{
				go.transform.position = position;
				TemporaryManager.Instance.Add(go);

				Ball mainBall = go.GetComponent<Ball>();

				if (mainBall != null)
				{
					mainBall.startDirection = Ball.Direction.Forward;
					mainBall.Stop(); // Wait for launch
					AddBall(mainBall);
				}
			}
			return go;
		}

		void DesktopInput()
		{
			// Handle click/tap in different states
			if (CanClickOrTapOnPaddle)
			{
				// Left Ctrl or Space keys
				if (Input.GetButtonUp("Submit"))
					HandleClickOrTapOnPaddle();
			}

			// Paddle movement: left, right
			if (paddle.CanMove)
				paddle.Move(Input.GetAxis("Horizontal"));
			
#if UNITY_EDITOR 
			// Some debug stuff in editor only
			if (Input.GetKeyUp(KeyCode.C))
				SkipLevel();

			if (Input.GetKeyUp(KeyCode.V))
				CaptureScreenshot();
#endif
		}

		void CaptureScreenshot()
		{
			if (LevelManager.Instance.LevelWasSet)
			{
				string fileName = "Screenshot_{0}.png";
				ScreenCapture.CaptureScreenshot(string.Format(fileName, LevelManager.Instance.CurrentIndex), 0);			
			}
		}

		void MobileInput()
		{
			if (Input.touchCount > 0 && !InputManager.Instance.IsAnyTouchOverUI())
			{
				// Looking for a "began" touch
				if (InputManager.Instance.HasBeganTouch())
				{
					Touch? touch = InputManager.Instance.GetLastBeganTouch();

					if (touch.HasValue)
					{
						fingerId = touch.Value.fingerId;

						// Handle click/tap (on paddle) in different states
						// NOTE: touch must be on the paddle
						if (CanClickOrTapOnPaddle &&
							InputManager.Instance.Raycast(touch.Value.position, DisplayManager.Instance.CurrentCamera, paddle.gameObject, Constants.LayerMasks.Actors))
						{
							HandleClickOrTapOnPaddle();
							fingerId = -1;
						}
					}
				}

				if (fingerId > -1)
				{
					Touch? touch = InputManager.Instance.GetTouchByFingerId(fingerId);

					if (touch.HasValue)
					{
						switch (touch.Value.phase)
						{
							// Paddle movement: left, right
							// NOTE: micro movements in touch can break stationary state, so we handle Moved as Stationary
							case TouchPhase.Moved:
							case TouchPhase.Stationary:
								if (paddle.CanMove)
								{
									Rect leftRect = new Rect(0, 0, Screen.width * 0.5f, Screen.height);
									float horizontal = leftRect.Contains(touch.Value.position) ? -touchSensitivity : touchSensitivity;
									horizontal *= Time.deltaTime;
									paddle.Move(horizontal);
								}
								break;

							// Drop current touch and wait for a next one to begin
							case TouchPhase.Ended:
							case TouchPhase.Canceled:
								fingerId = -1;
								break;
						}
					}
					else
					{
						fingerId = -1;
					}
				}
			}
			else
			{
				fingerId = -1;
			}
		}

		void HandleClickOrTapOnPaddle()
		{
			iTween.PunchScale(paddle.gameObject, new Vector3(0.125f, 0.125f, 0.125f), 1f);
			switch (state)
			{
				case GameState.WaitForTryAgain:
				{
					GUIManager.Instance.HideLargeText();
					if (!HasBall)
						CreateMainBall(Vector3.zero);
					MainBall.Stop();
					NormalBallSpeed();
					paddle.AttachBall(MainBall, true);
					paddle.SetText(Constants.Texts.Launch);
					paddle.SetTextVisible(true);
					SetState(GameState.WaitForLaunch);
				}
				break;

				case GameState.WaitForLaunch:
				case GameState.PlayingAndWaitForLaunch:
				{
					if (GUIManager.Instance.HelpIconsVisible)
						GUIManager.Instance.HideHelpIcons();
					AudioManager.Instance.Stop("Ready Signal Loop");
					AudioManager.Instance.PlaySound("Launch");
					if (!AudioManager.Instance.IsPlaying("Music"))
						AudioManager.Instance.PlayMusic("Music");
					
					// Get the currently attahced ball
					GUIManager.Instance.HideLargeText();

					Ball attachedBall = paddle.AttachedBall;
					paddle.DetachBall();
					paddle.SetText(string.Empty);
					paddle.SetTextVisible(false);
					// Launch the previously attached ball
					if (attachedBall != null)
						attachedBall.Play();
					else
						MainBall.Play();
					SetState(GameState.Playing);
				}
				break;

				case GameState.LevelDone:
				{
					Cleanup();
					NextLevel();
				}
				break;
			}
		}

		#endregion

		#region Event handlers

		void OnLoadTitle()
		{
			SceneManager.LoadScene(Constants.Scenes.Title);
		}
		
		void OnLoadGameOver()
		{
			SceneManager.LoadScene(Constants.Scenes.GameOver);
		}
		
		#endregion
	}
}
