﻿using UnityEngine;

namespace Konyisoft
{
	public class FPSManager : Singleton<FPSManager>
	{
		#region Fields and properties

		public float updateInterval = 0.5f;
		[Range(1, 100)]
		public int targetFrameRate = 60;

		public float FPS { get { return fps; } }
		public string FPSAsString { get { return fps.ToString("f2"); } }

		float accum = 0f;
		int frames = 0;
		float timeLeft = 0f;
		float fps = 0f;
		float lastSample = 0f;

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			useGUILayout = false;
			timeLeft = updateInterval;
			lastSample = Time.realtimeSinceStartup;
		}

		void Update()
		{
			frames++;
			float newSample = Time.realtimeSinceStartup;
			float deltaTime = newSample - lastSample;
			lastSample = newSample;

			timeLeft -= deltaTime;
			accum += 1.0f / deltaTime;

			if (timeLeft <= 0f)
			{
				fps = accum / frames;
				timeLeft = updateInterval;
				accum = 0f;
				frames = 0;
			}
		}

		#endregion
	}
}
