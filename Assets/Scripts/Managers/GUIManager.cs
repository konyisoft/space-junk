using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Konyisoft
{
	public class GUIManager : Singleton<GUIManager>
	{
		#region Fields and properties

		public bool HelpIconsVisible
		{
			get { return helpIconsVisible; }
		}

		public bool QuitPanelVisible
		{
			get { return quitPanelVisible; }
		}
		
		public Canvas canvas;
		public Text livesText;
		public Text scoreText;
		public Text largeText;
		public Text fpsText;
		public CanvasRenderer helpIcons;
		public CanvasRenderer quitPanel;
		
		public float flashDuration = 0.35f;
		public Color flashStartColor = Color.white;
		public Color flashEndColor = Color.blue; // 54, 172, 255
		
		public float largetTextFadeDuration = 1f;
		public float largetTextHideDelay = 1f;
		
		Text[] flashingTexts;
		FloatingText floatingText;
		float timer = 0.0f; 
		List<Led> leds = new List<Led>();
		bool helpIconsVisible = false;
		bool quitPanelVisible = false;
		
		#endregion
		
		#region Mono methods
		
		protected override void Awake()
		{
			base.Awake();
			flashingTexts = new Text[] { livesText, scoreText };
			if (canvas != null)
				leds = canvas.GetComponentsInChildren<Led>().ToList();
			HideLargeText(true);
			HideQuitPanel();
		}
		
		void Update()
		{
			// Flashing texts
			timer += Time.deltaTime;
			float t = Mathf.PingPong(timer, flashDuration) / flashDuration;
			for (int i = 0; i < flashingTexts.Length; i++)
			{
				if (flashingTexts[i] != null)
					flashingTexts[i].color = Color.Lerp(flashStartColor, flashEndColor, t);
			}
			
			// Show FPS
			if (fpsText != null)
				fpsText.text = FPSManager.Instance.FPSAsString;
		}
		
		#endregion
		
		#region Public methods

		public void WriteLives(int lives)
		{
			if (livesText != null)
				livesText.text = lives.ToString();
		}
		
		public void WriteScore(int score)
		{
			if (scoreText != null)
				scoreText.text = string.Format("{0:D8}", score);
		}
		
		public void ShowLargeText(string text, bool autohide = false)
		{
			if (largeText != null)  // No activeInHierarchy condition needed
			{
				StopAllCoroutines();
				largeText.gameObject.SetActive(true);  // true any way
				largeText.text = text;
				StartCoroutine(CO_ShowLargeText(autohide));
			}
		}

		public void HideLargeText(bool immediate = false)
		{
			if (largeText != null && largeText.gameObject.activeInHierarchy)
			{
				StopAllCoroutines();
				if (immediate)
				{
					SetLargeTextScale(Vector3.zero);
					largeText.text = string.Empty;
					largeText.gameObject.SetActive(false);
				}
				else
				{
					StartCoroutine(CO_HideLargeText());
				}
			}
		}
		
		public void InvokeFloatingText(string text)
		{
			if (canvas != null)
			{
				// Simple concatenation of the passed text
				if (floatingText != null && floatingText.Alpha < 0.5f)
				{
					floatingText.Text = floatingText.Text + "\n" + text;
				}
				// Instantiating a new one
				else
				{
					GameObject textPrefab = ResourceManager.Instance.GetPrefab("Texts/FloatingText");
					if (textPrefab != null)
					{
						GameObject textObject = Instantiate(textPrefab);
						textObject.transform.SetParent(canvas.transform);
						textObject.transform.localPosition = new Vector3(0, -140, 0);
						textObject.transform.localRotation = Quaternion.identity;
						textObject.transform.localScale = Vector3.one;
						FloatingText ft = textObject.GetComponent<FloatingText>();
						if (ft != null)
						{
							ft.Text = text;
							floatingText = ft;
						}	
					}
				}
			}
		}
		
		public void ClearFloatingText()
		{
			floatingText = null;
		}
		
		public void TurnOnLed(PowerUpManager.PowerUpType powerUpType)
		{
			Led led = leds.Find(l => l.powerUpType == powerUpType);
			if (led != null)
				led.TurnOn();
		}

		public void TurnOffLed(PowerUpManager.PowerUpType powerUpType)
		{
			Led led = leds.Find(l => l.powerUpType == powerUpType);
			if (led != null)
				led.TurnOff();
		}

		public void BlinkLed(PowerUpManager.PowerUpType powerUpType)
		{
			Led led = leds.Find(l => l.powerUpType == powerUpType);
			if (led != null && !led.IsBlinking)
				led.Blink();
		}
		
		public void TurnOffAllLeds()
		{
			leds.ForEach(led => led.TurnOff());
		}

		public void ShowHelpIcons()
		{
			if (helpIcons != null && !helpIconsVisible)
			{
				helpIcons.gameObject.SetActive(true);
				helpIconsVisible = true;
			}
		}

		public void HideHelpIcons()
		{
			if (helpIcons != null && helpIconsVisible)
			{
				helpIcons.gameObject.SetActive(false);
				helpIconsVisible = false;
			}
		}

		public void ShowQuitPanel()
		{
			if (quitPanel != null && !quitPanelVisible)
			{
				quitPanel.gameObject.SetActive(true);
				Time.timeScale = 0f;
				quitPanelVisible = true;
			}
		}
		
		public void HideQuitPanel()
		{
			if (quitPanel != null && quitPanelVisible)
			{
				quitPanel.gameObject.SetActive(false);
				Time.timeScale = 1f;
				quitPanelVisible = false;
			}
		}
		
		#endregion
		
		#region Private methods
		
		void SetLargeTextScale(Vector3 scale)
		{
			if (largeText != null)
			{
				RectTransform rt = largeText.transform as RectTransform;
				rt.localScale = scale;
			}
		}
		
		IEnumerator CO_ShowLargeText(bool autohide = false)
		{
			yield return StartCoroutine(CoroutineUtils.CO_LerpVector3(
				new Vector3(0.35f, 0.35f, 0.35f),
				Vector3.one,
				largetTextFadeDuration,
				(x) => SetLargeTextScale(x)
			));
			
			if (autohide)
			{
				yield return new WaitForSeconds(largetTextHideDelay);
				yield return StartCoroutine(CO_HideLargeText());
			}
		}

		IEnumerator CO_HideLargeText()
		{
			yield return StartCoroutine(CoroutineUtils.CO_LerpVector3(
				largeText.transform.localScale,
				new Vector3(0.35f, 0.35f, 0.35f),
				largetTextFadeDuration,
				(x) => SetLargeTextScale(x)
			));

			largeText.text = string.Empty;
			largeText.gameObject.SetActive(false);
		}
		
		#endregion
		
		#region Event handlers
		
		public void OnPointerUp_ProjectionIcon()
		{
			if (GameManager.Instance.State != GameManager.GameState.WonTheGame)
				DisplayManager.Instance.ToggleProjection();
		}
		
		public void OnPointerUp_QuitIcon()
		{
			if (GameManager.Instance.State != GameManager.GameState.WonTheGame)
				ShowQuitPanel();
		}
		
		public void OnClick_QuitButtonYes()
		{
			//HideQuitPanel();
			AudioManager.Instance.PlaySound("Click");
			Time.timeScale = 1f;
			GameManager.Instance.LoadTitle();
		}
		
		public void OnClick_QuitButtonNo()
		{
			AudioManager.Instance.PlaySound("Click");
			HideQuitPanel();
		}
		
		
		#endregion
	}
}