using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Konyisoft
{
	public class PowerUpManager : Singleton<PowerUpManager>
	{
		#region Enumerations
		
		public enum PowerUpType
		{
			Unset,
			AdditionalBall,
			SmallPaddle,
			LargePaddle,
			StickingBall,
			SpeedUp,
			SpeedDown,
			InverseControl,
			ExtraLife,
			Score500,
			Score1000,
			Score250,
		}
		
		#endregion 
		
		#region Classes and structs
		
		class PowerUpEffect
		{
			public PowerUpType type;
			public float duration;
			
			public PowerUpEffect(PowerUpType type, float duration)
			{
				this.type = type;
				this.duration = duration;
			}
		}
		
		#endregion
		
		#region Fields and properties

		// Powerups on the field
		List<PowerUp> instantiated = new List<PowerUp>();
		// Contains the currently applied powerups' durations
		List<PowerUpEffect> appliedEffects = new List<PowerUpEffect>();

		#endregion
		
		#region Mono methods

		void Update()
		{
			// Works in playing states only
			if (GameManager.Instance.State == GameManager.GameState.Playing ||
				GameManager.Instance.State == GameManager.GameState.PlayingAndWaitForLaunch)
			{
				// Decrease duration time on all applied powerups
				for (int i = appliedEffects.Count - 1; i >= 0; i--)  // Reversed iteration
				{
					appliedEffects[i].duration -= Time.deltaTime;
					
					// Below 4 secs paddlde leds start to blink
					if (appliedEffects[i].duration <= 4f)
					{
						// TODO: not optimized :(
						GUIManager.Instance.BlinkLed(appliedEffects[i].type);
					}
					// Delete ones with 0 duration
					if (appliedEffects[i].duration <= 0f)
					{
						RevertEffect(appliedEffects[i].type);
						appliedEffects.RemoveAt(i);
					}
				}
			}
		}
		
		#endregion
		
		#region Public methods
	
		public GameObject InstantiatePowerUp(PowerUpType powerUpType, Vector3 position)
		{
			// These types won't be instantiated
			if (powerUpType == PowerUpType.Unset)
				return null;
			
			GameObject go = Instantiate(ResourceManager.Instance.GetPrefab("PowerUps/" + powerUpType.ToString()));
			if (go != null)
			{
				go.transform.position = position;
				TemporaryManager.Instance.Add(go);
				HandleSpecialPowerUp(powerUpType, go);
			}
			return go;
		}
		
		public void RegisterPowerUp(PowerUp powerUp)
		{
			instantiated.Add(powerUp);
		}

		public void UnregisterPowerUp(PowerUp powerUp)
		{
			if (instantiated.Contains(powerUp))
				instantiated.Remove(powerUp);
		}
		
		public void ClearAppliedEffects()
		{
			appliedEffects.Clear();
		}
		
		public void EvaporateAll()
		{
			instantiated.ForEach(powerUp => powerUp.Evaporate());
		}

		public bool IsEffectApplied(PowerUpType powerUpType)
		{
			return appliedEffects.FindIndex(effect => effect.type == powerUpType) > -1;
		}

		#endregion
		
		#region Internal methods
		
		internal void ApplyEffect(PowerUpType powerUpType)
		{
			int index = appliedEffects.FindIndex(effect => effect.type == powerUpType);

			// Currently active? Maximize its duration
			if (index >= 0)
			{
				appliedEffects[index].duration = Constants.Game.EffectsDuration;
				GUIManager.Instance.TurnOnLed(powerUpType); // Turn on its led (if it is blinking)
			}	
			// Not active yet: add powerup to the list
			else
			{
				// Add to the list
				appliedEffects.Add(new PowerUpEffect(powerUpType, Constants.Game.EffectsDuration));
				ActivateEffect(powerUpType);
			}
		}

		internal void ActivateEffect(PowerUpType powerUpType)
		{
			// Do the appropriate effect
			switch (powerUpType)
			{
				case PowerUpType.SmallPaddle:
					RemoveAppliedEffect(PowerUpType.LargePaddle);
					GameManager.Instance.paddle.SmallSize();
					GUIManager.Instance.TurnOnLed(powerUpType);
					GUIManager.Instance.TurnOffLed(PowerUpType.LargePaddle);
					break;
					
				case PowerUpType.LargePaddle:
					RemoveAppliedEffect(PowerUpType.SmallPaddle);
					GameManager.Instance.paddle.LargeSize();
					GUIManager.Instance.TurnOnLed(powerUpType);
					GUIManager.Instance.TurnOffLed(PowerUpType.SmallPaddle);
					break;
					
				case PowerUpType.StickingBall:
					GUIManager.Instance.TurnOnLed(powerUpType);
					break;
					
				case PowerUpType.SpeedDown:
					RemoveAppliedEffect(PowerUpType.SpeedUp);
					GameManager.Instance.SpeedDownBall();
					GUIManager.Instance.TurnOnLed(powerUpType);
					GUIManager.Instance.TurnOffLed(PowerUpType.SpeedUp);
					break;
					
				case PowerUpType.SpeedUp:
					RemoveAppliedEffect(PowerUpType.SpeedDown);
					GameManager.Instance.SpeedUpBall();
					GUIManager.Instance.TurnOnLed(powerUpType);
					GUIManager.Instance.TurnOffLed(PowerUpType.SpeedDown);
					break;

				case PowerUpType.InverseControl:
					GameManager.Instance.paddle.InverseControl();
					GUIManager.Instance.TurnOnLed(powerUpType);
					break;

				case PowerUpType.ExtraLife:
					GameManager.Instance.IncreaseLife(1);
					break;

				case PowerUpType.Score250:
					GameManager.Instance.IncreaseScore(250);
					break;
					
				case PowerUpType.Score500:
					GameManager.Instance.IncreaseScore(500);
					break;

				case PowerUpType.Score1000:
					GameManager.Instance.IncreaseScore(1000);
					break;
			}
		}
		
		#endregion
		
		#region Private methods

		void HandleSpecialPowerUp(PowerUpType powerUpType, GameObject go)
		{
			switch (powerUpType)
			{
				case PowerUpType.AdditionalBall:
					// Create and start additional ball
					Ball additionalBall = go.GetComponent<Ball>();
					if (additionalBall != null)
					{
						additionalBall.startDirection = Ball.Direction.Backward;
						additionalBall.Play();
						// Change direction to behave as a normal ball (after Play() executed)
						additionalBall.startDirection = Ball.Direction.Forward;
						GameManager.Instance.AddBall(additionalBall);
						// Set the same speed as the main ball has (SpeedUp, SpeedDown)
						additionalBall.CurrentSpeed = GameManager.Instance.MainBall.CurrentSpeed;
						// GUI text
						GUIManager.Instance.InvokeFloatingText(Constants.Texts.PowerUps[PowerUpType.AdditionalBall]);
					}
				break;
			}
		}
		
		void RevertEffect(PowerUpType powerUpType)
		{
			switch (powerUpType)
			{
				case PowerUpType.SmallPaddle:
					GameManager.Instance.paddle.NormalSize();
					GUIManager.Instance.TurnOffLed(powerUpType);
					break;
					
				case PowerUpType.LargePaddle:
					GameManager.Instance.paddle.NormalSize();
					GUIManager.Instance.TurnOffLed(powerUpType);
					break;
					
				case PowerUpType.StickingBall:
					GUIManager.Instance.TurnOffLed(powerUpType);
					GameManager.Instance.ForceClickOrTapOnPaddle();
					break;
					
				case PowerUpType.SpeedDown:
					GameManager.Instance.NormalBallSpeed();
					GUIManager.Instance.TurnOffLed(powerUpType);
					break;
					
				case PowerUpType.SpeedUp:
					GameManager.Instance.NormalBallSpeed();
					GUIManager.Instance.TurnOffLed(powerUpType);
					break;

				case PowerUpType.InverseControl:
					GameManager.Instance.paddle.NormalControl();
					GUIManager.Instance.TurnOffLed(powerUpType);
					break;
			}
		}
		
		void RemoveAppliedEffect(PowerUpType powerUpType)
		{
			int index = appliedEffects.FindIndex(effect => effect.type == powerUpType);
			if (index >= 0)
				appliedEffects.RemoveAt(index);
		}
		
		#endregion
	}
}