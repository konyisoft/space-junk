using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Konyisoft
{
	public class HighscoreManager : Singleton<HighscoreManager>
	{
		#region Fields and proprties

		public Camera mainCamera;
		public Camera starfieldCamera;
		public Camera backgroundCamera;
		
		public CanvasRenderer nameLetters;
		public CanvasRenderer keyboard;
		
		public float flashDuration = 0.35f;
		public Color flashStartColor = Color.white;
		public Color flashEndColor = Color.blue; // 54, 172, 255
		
		public Sprite letterNormal;
		public Sprite letterHighlight;
		
		List<Text> letters = new List<Text>();  // Sorted by name
		
		float flashTimer = 0f; 
		int letterIndex = 0;
		
		// NOTE: naming conventions on these buttons
		const string KeySpace = "spc";
		const string KeyBack = "bck";
		const string KeyEnd = "end";
		
		const string Space = " ";
		
		#endregion
		
		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			
			AspectRatio.Instance.AddCamera(mainCamera, starfieldCamera, backgroundCamera);
			Screen.sleepTimeout = SleepTimeout.NeverSleep;

			Globals.LoadPlayerPrefs();
			
			// Setup keyboard
			if (keyboard != null)
			{
				List<Button> buttons = keyboard.GetComponentsInChildren<Button>().ToList();
				buttons.ForEach(button =>
				{
					button.onClick.AddListener(delegate { OnClick_Button(button); });
				});
			}
			
			// Setup letters
			if (nameLetters != null)
			{
				// Get letters ordered by name (number)
				letters = nameLetters.GetComponentsInChildren<Text>()
					.OrderBy(letter => letter.gameObject.name)
					.ToList();
					
				ClearLetters();
				letterIndex = 0;
				
				// Display last name typed
				if (!string.IsNullOrEmpty(Globals.lastName))
					DisplayText(Globals.lastName);
				
				HighlightLetter(letterIndex);
			}
		}
		
		void Start()
		{
			ScreenFader.Instance.FadeIn();
		}
		
		void Update()
		{
			// Flashing letters
			flashTimer += Time.deltaTime;
			float t = Mathf.PingPong(flashTimer, flashDuration) / flashDuration;
			for (int i = 0; i < letters.Count; i++)
			{
				if (letters[i] != null)
					letters[i].color = Color.Lerp(flashStartColor, flashEndColor, t);
			}
			
			// Screen fader 
			if (ScreenFader.Instance.IsFading)
				return;

			// Common input
			if (Input.GetButtonUp("Cancel") || Input.GetKeyUp(KeyCode.Escape))
				End();
		}

		#endregion
		
		#region Public methods

		public void LoadTitle()
		{
			Globals.titleMode = TitleManager.Mode.Highscore;
			ScreenFader.Instance.FadeOut(OnLoadTitle);
		}
		
		#endregion
		
		#region Private methods
		
		void ClearLetters()
		{
			letters.ForEach(letter =>
			{
				letter.text = Space; // Fills with spaces
			});
		}
		
		void DisplayText(string text)
		{
			ClearLetters(); // Clear first
			
			text = text.TrimEnd(); // Important to trim spaces from end
			
			for (int i = 0; i < text.Length; i++)
			{
				if (i == letters.Count)  // Won't overflow
					break;
				
				letters[i].text = text[i].ToString().ToUpper();
				letterIndex = i + 1;
			}
		}
		
		void End()
		{
			// Get the name and save highscore here
			string theName = GetStringFromLetters();
			Globals.lastName = theName;
			Highscore.AddScore(Globals.lastName, Globals.lastScore, true);
			LoadTitle();
		}

		#endregion
		
		#region Event handlers
		
		void OnLoadTitle()
		{
			SceneManager.LoadScene(Constants.Scenes.Title);
		}
		
		void OnClick_Button(Button sender)
		{
			if (sender != null)
			{
				Text text = sender.GetComponentInChildren<Text>();
				if (text != null)
				{
					AudioManager.Instance.PlaySound("Click");
					
					// Backspace
					if (sender.gameObject.name == KeyBack)
					{
						if (letterIndex > 0)
						{
							letters[letterIndex - 1].text = Space;
							if (letterIndex <= Highscore.MaxLetters)
								NormalLetter(letterIndex);
							letterIndex--;
							HighlightLetter(letterIndex);
						}
					}
					// End
					else if (sender.gameObject.name == KeyEnd)
					{
						End();
					}
					// Space or other character
					else if (letterIndex <= Highscore.MaxLetters)
					{
						if (sender.gameObject.name == KeySpace)
							letters[letterIndex].text = Space;
						else
							letters[letterIndex].text = text.text.ToUpper();
						
						NormalLetter(letterIndex);
						
						// Incresase index
						letterIndex++; // Intended overflow, LOL (MaxLetters + 1)
						
						if (letterIndex <= Highscore.MaxLetters)
							HighlightLetter(letterIndex);
					}
				}
			}
		}

		void NormalLetter(int index)
		{
			SetLetterSprite(index, letterNormal);
		}
		
		void HighlightLetter(int index)
		{
			SetLetterSprite(index, letterHighlight);
		}

		void SetLetterSprite(int index, Sprite sprite)
		{
			if (index >= 0 && index <= Highscore.MaxLetters)
			{
				Image image = letters[index].GetComponentInParent<Image>();
				if (image != null)
					image.sprite = sprite;
			}
		}
		
		string GetStringFromLetters()
		{
			string s = string.Empty;
			for (int i = 0; i < letters.Count; i++)
			{
				s += letters[i].text;
			}
			return s;
		}

		#endregion
	}
}
