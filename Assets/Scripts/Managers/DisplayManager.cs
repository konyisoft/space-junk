using UnityEngine;
using UnityEngine.UI;

namespace Konyisoft
{
	public class DisplayManager : Singleton<DisplayManager>
	{
		#region Enumerations
		
		public enum Projection
		{
			Isometric,
			Perspective
		}
		
		#endregion
		
		#region Fields and proprties

		public Camera CurrentCamera
		{
			get
			{
				return projection == Projection.Isometric ? isometricCamera : perspectiveCamera;
			}
		}
		
		public Camera isometricCamera;
		public Camera perspectiveCamera;
		public Camera starfieldCamera;
		public Camera backgroundCamera;
		public Canvas canvas;
		public Projection projection = Projection.Isometric;
		public Background background;
		
		Projection lastProjection = Projection.Isometric;
		
		#endregion
		
		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			AspectRatio.Instance.AddCamera(isometricCamera, perspectiveCamera, starfieldCamera, backgroundCamera);
			Screen.sleepTimeout = SleepTimeout.NeverSleep;
			
			SetProjection(projection);
		}
		
		void Update()
		{
			// Changing projection
			if (projection != lastProjection)
				SetProjection(projection);
		}
		
		#endregion
		
		#region Public methods
		
		public void SetProjection(Projection projection)
		{
			switch (projection)
			{
				case Projection.Isometric:
					SetIsometricCamera();
				break;
				case Projection.Perspective:
					SetPerspectiveCamera();
				break;
			}
		}

		public void ToggleProjection()
		{
			if (projection == Projection.Isometric)
				SetPerspectiveCamera();
			else
				SetIsometricCamera();
		}
		
		#endregion
		
		#region Private methods
		
		void SetIsometricCamera()
		{
			if (isometricCamera != null && perspectiveCamera != null && canvas != null)
			{
				isometricCamera.gameObject.tag = "MainCamera";
				isometricCamera.gameObject.SetActive(true);
				
				perspectiveCamera.gameObject.tag = "Untagged";
				perspectiveCamera.gameObject.SetActive(false);

				canvas.worldCamera = isometricCamera;
				
				projection = Projection.Isometric;
				lastProjection = projection;
			}
		}

		void SetPerspectiveCamera()
		{
			if (isometricCamera != null && perspectiveCamera != null && canvas != null)
			{
				perspectiveCamera.gameObject.tag = "MainCamera";
				perspectiveCamera.gameObject.SetActive(true);
				
				isometricCamera.gameObject.tag = "Untagged";
				isometricCamera.gameObject.SetActive(false);
				
				canvas.worldCamera = perspectiveCamera;
				
				projection = Projection.Perspective;
				lastProjection = projection;
			}
		}
		
		#endregion
	}
}
