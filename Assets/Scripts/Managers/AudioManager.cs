using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace Konyisoft
{
	public class AudioManager : Singleton<AudioManager>
	{
		#region Classes and structs
		
		public class AudioData
		{
			public AudioSource audioSource;
			public float origVolume = 1f;
		}
		
		#endregion
		
		#region Fields and properties
		
		public GameObject audioParentObject;
		public bool mute = false;
		
		Dictionary<string, AudioData> cache = new Dictionary<string, AudioData>();
		bool preLoaded = false;
		
		#endregion
		
		#region Mono methods
		
		void OnDestroy()
		{
			StopAllCoroutines();
		}
		
		#endregion
		
		#region Public methods
		
		public void PreLoad()
		{
			if (!preLoaded && audioParentObject != null)
			{
				List<AudioSource> audioSources = audioParentObject.GetComponentsInChildren<AudioSource>().ToList();
				audioSources.ForEach(audioSource =>
				{
					AudioData data = new AudioData()
					{
						audioSource = audioSource,
						origVolume = audioSource.volume
					};
					AddToCache(audioSource.gameObject.name, data);
				});
				preLoaded = true;
			}
		}
		
		public void PlaySound(AudioData data, float fadeDuration)
		{
			if (Options.soundSetting == Options.SoundSetting.MusicAndSound ||
				Options.soundSetting == Options.SoundSetting.SoundOnly)
			{
				PlayInternal(data, fadeDuration);
			}
		}
		
		public void PlaySound(string name, float fadeDuration)
		{
			PlaySound(GetAudioData(name), fadeDuration);
		}
		
		public void PlaySound(string name)
		{
			PlaySound(name, 0f);
		}

		public void PlayMusic(AudioData data, float fadeDuration)
		{
			if (Options.soundSetting == Options.SoundSetting.MusicAndSound ||
				Options.soundSetting == Options.SoundSetting.MusicOnly)
			{
				PlayInternal(data, fadeDuration);
			}
		}
		
		public void PlayMusic(string name, float fadeDuration)
		{
			PlayMusic(GetAudioData(name), fadeDuration);
		}
		
		public void PlayMusic(string name)
		{
			PlayMusic(name, 0f);
		}

		public void Stop(AudioData data, float fadeDuration)
		{
			if (!mute && data != null && data.audioSource != null)
			{
				if (fadeDuration > 0f)
				{
					StopAllCoroutines();
					StartCoroutine(CO_StopWithFade(data, fadeDuration));
				}
				else
				{
					data.audioSource.Stop();
				}
			}
		}
		
		public void Stop(string name, float fadeDuration)
		{
			Stop(GetAudioData(name), fadeDuration);
		}
		
		public void Stop(string name)
		{
			Stop(name, 0f);
		}

		public bool IsPlaying(AudioData data)
		{
			return data != null && data.audioSource != null && data.audioSource.isPlaying;
		}

		public bool IsPlaying(string name)
		{
			return IsPlaying(GetAudioData(name));
		}

		public AudioData GetAudioData(string name)
		{
			// Try to get from cache
			if (cache.ContainsKey(name))
			{
				return cache[name];
			}
			// Otherwise try to find it
			else if (audioParentObject != null)
			{
				GameObject go = GameObject.Find(audioParentObject.name + "/" + name);
				if (go != null)
				{
					AudioSource audioSource = go.GetComponent<AudioSource>();
					AudioData data = new AudioData()
					{
						audioSource = audioSource,
						origVolume = audioSource.volume
					};
					AddToCache(name, data);  // Add to cache
					return data;
				}
			}
			return null;
		}
		
		#endregion
		
		#region Private methods
		
		void PlayInternal(AudioData data, float fadeDuration)
		{
			if (!mute && data != null && data.audioSource != null)
			{
				if (data.audioSource.isPlaying)
					data.audioSource.Stop();
				
				if (fadeDuration > 0f)
				{
					data.audioSource.volume = 0f;
					StartCoroutine(CO_Fade(data, 0f, data.origVolume, fadeDuration));
				}

				data.audioSource.Play();
			}
		}
		
		
		void AddToCache(string key, AudioData data)
		{
			if (!cache.ContainsKey(key))
				cache.Add(key, data);
		}
		
		IEnumerator CO_StopWithFade(AudioData data, float fadeDuration)
		{
			if (data != null && data.audioSource != null)
			{
				yield return StartCoroutine(CO_Fade(data, data.audioSource.volume, 0f, fadeDuration));
				data.audioSource.Stop();
				data.audioSource.volume = data.origVolume; // Reset volume after stop
			}
			yield return null;
		}	

		IEnumerator CO_Fade(AudioData data, float fromVolume, float toVolume, float duration)
		{
			if (data != null && data.audioSource != null)
			{
				float startTime = Time.time;
				while (Time.time - startTime < duration)
				{
					data.audioSource.volume = Mathf.Lerp(fromVolume, toVolume, (Time.time - startTime) / duration);
					yield return null;
				}
				data.audioSource.volume = toVolume;
			}
			yield return null;
		}
		
		#endregion

	}
}