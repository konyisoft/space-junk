using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Konyisoft
{
	public class GameOverManager : Singleton<GameOverManager>
	{
	#region Enumerations
		
		public enum Mode
		{
			GameOver,
			WonTheGame
		}
		
		#endregion

		#region Fields and proprties

		public Camera mainCamera;
		public Text gameOverText;
		public Text wonTheGameText;
	
		#endregion
		
		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			AspectRatio.Instance.AddCamera(mainCamera);
			Screen.sleepTimeout = SleepTimeout.NeverSleep;
		
			Globals.LoadPlayerPrefs();
		
			if (Globals.gameOverMode == Mode.WonTheGame)
				ShowWonTheGame();
			else 
				ShowGameOver();
			
			Globals.gameOverMode = Mode.GameOver; // Set to default
		}
		
		void Start()
		{
			ScreenFader.Instance.FadeIn();
			AudioManager.Instance.PlaySound("Game Over Loop", 2f);
		}
		
		void Update()
		{
			// Screen fader 
			if (ScreenFader.Instance.IsFading)
				return;

			// Common input
			if (Input.GetButtonUp("Cancel") || Input.GetKeyUp(KeyCode.Escape))
				LoadHighscoreOrTitle();
				
			// Desktop input
#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBGL
				DesktopInput();
#endif

			// Mobile input
#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IPHONE)
				MobileInput();
#endif
		}

		#endregion
		
		#region Private methods
		
		void DesktopInput()
		{
			if (Input.GetButtonUp("Submit"))
				LoadHighscoreOrTitle();
		}

		void MobileInput()
		{
			if (Input.touchCount > 0 &&	Input.touches[0].phase == TouchPhase.Ended)
				LoadHighscoreOrTitle();
		}
		
		void LoadHighscoreOrTitle()
		{
			ScreenFader.Instance.FadeOut(OnLoadHighscoreOrTitle);
		}
		
		void ShowGameOver()
		{
			if (gameOverText != null)
				gameOverText.gameObject.SetActive(true);
			
			if (wonTheGameText != null)
				wonTheGameText.gameObject.SetActive(false);
		}
		
		void ShowWonTheGame()
		{
			if (gameOverText != null)
				gameOverText.gameObject.SetActive(false);
			
			if (wonTheGameText != null)
				wonTheGameText.gameObject.SetActive(true);
		}

		#endregion
		
		#region Event handlers
		
		void OnLoadHighscoreOrTitle()
		{
			// NOTE: Obsolete! use UnityEngine.SceneManagement and SceneManager.LoadScene
			// in newer version of Unity
			
			AudioManager.Instance.Stop("Game Over Loop", 0.4f);

			int highscorePosition = Highscore.GetPosition(Globals.lastScore);
			
			Globals.titleMode = TitleManager.Mode.Title; // May be changed if high score reached
			Globals.lastPosition = highscorePosition;
			
			if (highscorePosition > -1)
				SceneManager.LoadScene(Constants.Scenes.Highscore);
			else
				SceneManager.LoadScene(Constants.Scenes.Title);
		}

		#endregion
	}
}
