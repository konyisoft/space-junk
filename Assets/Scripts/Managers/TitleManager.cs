using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Konyisoft
{
	public class TitleManager : Singleton<TitleManager>
	{
		#region Enumerations

		public enum Mode
		{
			Title,
			Highscore
		}

		#endregion

		#region Fields and properties

		public Camera mainCamera;
		public Camera starfieldCamera;
		public Camera backgroundCamera;

		public CanvasRenderer titleScreen;
		public CanvasRenderer highscoreScreen;
		public Text soundSettingText;
		public Text resetText;
		public Button resetYesButton;
		public Button resetNoButton;

		public Text[] flashingTexts = new Text[0];
		public float flashDuration = 0.35f;
		public Color flashStartColor = Color.white;
		public Color flashEndColor = Color.blue; // 54, 172, 255

		public float screenSwitchInterval = 5f;

		float flashTimer = 0f;
		float switchTimer = 0f;
		bool isYesNoActive = false;
		bool skipInputForOneFrame = false;

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			AspectRatio.Instance.AddCamera(mainCamera, starfieldCamera, backgroundCamera);
			Screen.sleepTimeout = SleepTimeout.NeverSleep;

			Globals.LoadPlayerPrefs();

			RefreshGUI();
			SetYesNoActive(false);

			if (Globals.titleMode == Mode.Highscore)
				ShowHighscoreScreen();
			else
				ShowTitleScreen();

			Globals.titleMode = Mode.Title; // Set to default
		}

		void Start()
		{
			ScreenFader.Instance.FadeIn();
			AudioManager.Instance.PlayMusic("Title", 1.5f);
		}

		void Update()
		{
			// Flashing texts
			flashTimer += Time.deltaTime;
			float t = Mathf.PingPong(flashTimer, flashDuration) / flashDuration;
			for (int i = 0; i < flashingTexts.Length; i++)
			{
				if (flashingTexts[i] != null)
					flashingTexts[i].color = Color.Lerp(flashStartColor, flashEndColor, t);
			}

			// Screen fader (No updates during fade)
			if (ScreenFader.Instance.IsFading)
				return;

			// Quit an all platforms (except WebGL)
#if !UNITY_WEBGL
			if (Input.GetButtonUp("Cancel") || Input.GetKeyUp(KeyCode.Escape))
				QuitApp();
#endif

			// No further Update if Yes/No buttons are visible
			if (isYesNoActive)
				return;

			// Switch between screens			
			switchTimer += Time.deltaTime;
			if (switchTimer >= screenSwitchInterval)
			{
				if (titleScreen.gameObject.activeInHierarchy)
					ShowHighscoreScreen();
				else
					ShowTitleScreen();
			}

			// When clicking Yes or No buttons, we don't want another touch to be handled
			if (skipInputForOneFrame)
			{
				skipInputForOneFrame = false;
				return;
			}

			// Desktop input
#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBGL
			DesktopInput();
#endif

			// NOTE: most mobile input received via UI elements
#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IPHONE)
				MobileInput();
#endif
		}

		#endregion

		#region Private methods

		void RefreshGUI()
		{
			// Sound settings
			if (soundSettingText != null)
				soundSettingText.text = Constants.Texts.SoundSettings[Options.soundSetting];

			// High score
			if (highscoreScreen != null)
			{
				for (int i = 0; i < Highscore.Scores.Count; i++)
				{
					string path = "Highscore Panel/Row_" + (i + 1);
					Transform textTransform = highscoreScreen.transform.Find(path + "/Name");
					if (textTransform != null)
						textTransform.GetComponent<Text>().text = Highscore.Scores[i].name.ToUpper();

					Transform scoreTransform = highscoreScreen.transform.Find(path + "/Score");
					if (scoreTransform != null)
						scoreTransform.GetComponent<Text>().text = string.Format("{0:D8}", Highscore.Scores[i].value);
				}
			}
		}

		void DesktopInput()
		{
			// Left Ctrl or Space keys
			if (Input.GetButtonUp("Submit"))
			{
				if (titleScreen.gameObject.activeInHierarchy)
					OnClick_StartGame();
				else
					ShowTitleScreen();
			}
			else if (Input.GetMouseButtonUp(0) && !titleScreen.gameObject.activeInHierarchy)
			{
				ShowTitleScreen();
			}
		}

		void MobileInput()
		{
			if (!titleScreen.gameObject.activeInHierarchy &&
				Input.touchCount > 0 &&
				Input.touches[0].phase == TouchPhase.Ended)
			{
				ShowTitleScreen();
			}
		}

		void ShowTitleScreen()
		{
			switchTimer = 0f;
			titleScreen.gameObject.SetActive(true);
			highscoreScreen.gameObject.SetActive(false);
		}

		void ShowHighscoreScreen()
		{
			switchTimer = 0f;

			// Show the last high score highlighted
			if (Globals.lastPosition > -1 && highscoreScreen != null)
			{
				string path = "Highscore Panel/Row_" + (Globals.lastPosition + 1);
				Transform textParent = highscoreScreen.transform.Find(path);
				if (textParent != null)
				{
					List<Text> flashingTextsList = flashingTexts.ToList(); // Some conversion
					textParent.GetComponentsInChildren<Text>().ToList().ForEach(text =>
					{
						flashingTextsList.Add(text);
					});
					flashingTexts = flashingTextsList.ToArray(); // Convert back to array
				}
			}

			titleScreen.gameObject.SetActive(false);
			highscoreScreen.gameObject.SetActive(true);
		}

		void QuitApp()
		{
			ScreenFader.Instance.FadeOut(OnQuitApp);
		}

		void SetYesNoActive(bool active)
		{
			if (resetText != null && resetYesButton != null && resetNoButton != null)
			{
				resetText.text = active ? Constants.Texts.AreYouSure : Constants.Texts.Reset;
				resetYesButton.gameObject.SetActive(active);
				resetNoButton.gameObject.SetActive(active);
				isYesNoActive = active;
				switchTimer = 0f;
			}
		}

		#endregion

		#region Event handlers

		public void OnClick_StartGame()
		{
			Globals.startLevelIndex = 0;
			AudioManager.Instance.PlaySound("Click");
			AudioManager.Instance.Stop("Title", 0.3f);
			ScreenFader.Instance.FadeOut(OnStartGame);
		}

		public void OnClick_SoundSettings()
		{
			AudioManager.Instance.PlaySound("Click");
			switchTimer = 0f;

			if ((int)Options.soundSetting < 4)
				Options.soundSetting++;
			else
				Options.soundSetting = (Options.SoundSetting)1;

			Options.Save();
			RefreshGUI();

			// TODO: This is kinda hack.
			// Implement a correct AudionManager later.
			if (Options.soundSetting == Options.SoundSetting.MusicAndSound || Options.soundSetting == Options.SoundSetting.MusicOnly)
			{
				if (!AudioManager.Instance.IsPlaying("Title"))
					AudioManager.Instance.PlayMusic("Title", 1.5f);
			}
			else if (Options.soundSetting == Options.SoundSetting.SoundOnly || Options.soundSetting == Options.SoundSetting.None)
			{
				if (AudioManager.Instance.IsPlaying("Title"))
					AudioManager.Instance.Stop("Title");
			}
		}

		public void OnClick_Levels()
		{
			AudioManager.Instance.PlaySound("Click");
			ScreenFader.Instance.FadeOut(OnLevels);
		}

		public void OnClick_ResetHighscore()
		{
			SetYesNoActive(true);
			skipInputForOneFrame = true;
		}

		public void OnClick_ResetYes()
		{
			AudioManager.Instance.PlaySound("Click");
			Highscore.SaveDefault();
			RefreshGUI();
			SetYesNoActive(false);
			skipInputForOneFrame = true;
		}

		public void OnClick_ResetNo()
		{
			AudioManager.Instance.PlaySound("Click");
			SetYesNoActive(false);
			skipInputForOneFrame = true;
		}

		void OnStartGame()
		{
			SceneManager.LoadScene(Constants.Scenes.Game);
		}

		void OnLevels()
		{
			SceneManager.LoadScene(Constants.Scenes.Levels);
		}

		void OnQuitApp()
		{
			Application.Quit();
		}

		#endregion
	}
}
