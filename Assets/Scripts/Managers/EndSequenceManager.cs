using System.Linq;
using System.Collections;
using UnityEngine;

namespace Konyisoft
{
	public class EndSequenceManager : Singleton<EndSequenceManager>
	{
		#region Enumerations

		#endregion

		#region Fields and properties

		public bool IsPlaying
		{
			get { return isPlaying; }
		}
		
		public GameObject environment;
		public GameObject fakeEnvironment;
		public RotateTransform backgroundRotator;
		public float velocity = 10f;
		public FloatRange randomVelocity;
		public FloatRange angularVelocity;
		
		bool isPlaying = false;
		bool isRotating = false;
		
		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			
			if (environment == null)
				Debug.LogError("No environment object attached.");
			if (fakeEnvironment == null)
				Debug.LogError("No fake environment object attached.");
			if (backgroundRotator == null)
				Debug.LogError("No background rotator object attached.");
			
			fakeEnvironment.SetActive(false);
		}
		
		void Update()
		{
			// Increasing background rotation speed
			if (isRotating)
			{
				Vector3 speed = backgroundRotator.speed;
				speed.z -= Time.deltaTime * 4f;
				backgroundRotator.speed = speed;
			}
		}

		#endregion

		#region Public methods

		public void PlaySequence()
		{
			StartCoroutine(CO_Shake());
			isPlaying = true;
			isRotating = true;
		}
		
		public void StopSequence()
		{
			StopAllCoroutines();
			isPlaying = false;
			isRotating = false;
		}
		
		#endregion

		#region Private methods

		IEnumerator CO_Shake()
		{
			yield return new WaitForSeconds(1.5f);

			AudioManager.Instance.PlaySound("Endseq Rising");
			
			// Shake
			Shaker shaker = environment.GetComponent<Shaker>();
			if (shaker != null)
				shaker.Shake();
			
			// Wait some seconds
			yield return new WaitForSeconds(7.5f);
			
			// Fade (explosion)
			ScreenFader.Instance.SetColor(Color.white);
			ScreenFader.Instance.FadeOut(OnFadedOut, 0.3f);
			
			AudioManager.Instance.PlaySound("Endseq Explosion");
		}

		IEnumerator CO_EndPlaying()
		{
			// Wait some seconds
			yield return new WaitForSeconds(4f);
			isPlaying = false;
			// Final fading out
			GameManager.Instance.EndSequenceEnded();
		}
		
		#endregion
		
		#region Event handlers
		
		void OnFadedOut()
		{
			// Do the explosion here ->
			
			// Disable original environment and enable the fake one
			environment.SetActive(false);
			fakeEnvironment.SetActive(true);
			GameManager.Instance.paddle.gameObject.SetActive(false);
			GameManager.Instance.Cleanup();  // Clear all
			
			// Look further
			DisplayManager.Instance.perspectiveCamera.farClipPlane = 250f;
			DisplayManager.Instance.isometricCamera.farClipPlane = 250f;
			
			// Background back to normal speed
			isRotating = false;			
			backgroundRotator.ResetSpeed();
			
			ScreenFader.Instance.FadeIn(OnFadedIn, 0.55f);
			
			// Create boom on fake environment
			Vector3 forward = DisplayManager.Instance.CurrentCamera.transform.forward;
			fakeEnvironment.GetComponentsInChildren<Rigidbody>().ToList().ForEach(r =>
			{
				r.velocity = forward * velocity + Random.onUnitSphere * randomVelocity.RandomInRange;
				r.angularVelocity = Random.onUnitSphere * angularVelocity.RandomInRange;					
			});
			
			// Wait for some time
			StartCoroutine(CO_EndPlaying());
		}
		
		void OnFadedIn()
		{
			ScreenFader.Instance.ResetColor();
		}
		
		#endregion
	}
}