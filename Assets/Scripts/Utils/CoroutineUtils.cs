﻿using System;
using System.Collections;
using UnityEngine;

public static class CoroutineUtils
{

	public static IEnumerator CO_LerpFloat(float fromFloat, float toFloat, float duration, Action<float> action)
	{
		if (action != null)
		{
			float startTime = Time.time;
			action(fromFloat);
			while (Time.time - startTime < duration)
			{
				action(Mathf.Lerp(fromFloat, toFloat, (Time.time - startTime) / duration));
				yield return null;
			}
			action(toFloat);
		}
		yield return null;
	}

	public static IEnumerator CO_LerpVector3(Vector3 fromVector3, Vector3 toVector3, float duration, Action<Vector3> action)
	{
		if (action != null)
		{
			float startTime = Time.time;
			action(fromVector3);
			while (Time.time - startTime < duration)
			{
				action(Vector3.Lerp(fromVector3, toVector3, (Time.time - startTime) / duration));
				yield return null;
			}
			action(toVector3);
		}
		yield return null;
	}

	public static IEnumerator CO_LerpColor(Color fromColor, Color toColor, float duration, Action<Color> action)
	{
		if (action != null)
		{
			float startTime = Time.time;
			action(fromColor);
			while (Time.time - startTime < duration)
			{
				action(Color.Lerp(fromColor, toColor, (Time.time - startTime) / duration));
				yield return null;
			}
			action(toColor);
		}
		yield return null;
	}



}
