using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Konyisoft
{
	public class Ripples : GameBehaviour
	{
		#region Fields and properties

		public bool IsFading
		{
			get { return isFading; }
		}
		
		public bool autoplay = true;
		public bool autodestroy = true;
		
		public float scaleSpeed = 1f;
		public float fadeInTime = 1f;
		public float fadeOutTime = 1f;
		public float upTime = 1f;
		
		Vector3 origScale;
		bool isFading = false;
		Material copyMaterial;
		
		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			
			if (HasMaterial)
			{
				copyMaterial = new Material(Renderer.material);
				Renderer.material = copyMaterial;
			}
			
			origScale = Transform.localScale;
			
			// Rotated in perspective view
			if (DisplayManager.Instance.projection == DisplayManager.Projection.Perspective)
				Transform.Rotate(0f, 180f, 0f);
			
			if (autoplay)
				Show();
			else
				Hide();
		}
		
		void Update()
		{
			Vector3 scale = Transform.localScale;
			scale.x += Time.deltaTime * scaleSpeed;
			scale.y = scale.x;
			Transform.localScale = scale;
		}
		
		void OnDestroy()
		{
			StopAllCoroutines();
			if (copyMaterial != null)
				Destroy(copyMaterial);
		}		
		
		#endregion
		
		#region Public methods
		
		public void Show()
		{
			Default();
			SetActive(true);
			StartCoroutine(CO_Fade());
		}
		
		public void Hide()
		{
			Default();
			SetActive(false);
		}
		
		public void Default()
		{
			StopAllCoroutines();
			SetAlpha(0f);
			isFading = false;
			Transform.localScale = origScale;
		}
		
		#endregion
		
		#region Private methods

		void SetActive(bool active)
		{
			gameObject.SetActive(active);
		}

		void SetAlpha(float alpha)
		{
			if (copyMaterial != null)
			{
				Color c = copyMaterial.color;
				c.a = alpha;
				copyMaterial.color = c;
			}
		}		
		
		IEnumerator CO_Fade()
		{
			isFading = true;
			yield return StartCoroutine(CoroutineUtils.CO_LerpFloat(0f, 0.5f, fadeInTime, (x) => SetAlpha(x)));
			yield return new WaitForSeconds(upTime);
			yield return StartCoroutine(CoroutineUtils.CO_LerpFloat(0.5f, 0f, fadeOutTime, (x) => SetAlpha(x)));
			isFading = false;

			if (autodestroy)
				Destroy(gameObject);
		}
		
		#endregion
	}
} 		
