﻿using UnityEngine;

namespace Konyisoft
{
	public class LevelData
	{
		#region Fields and properties

		public string id = string.Empty;  // Must be unique
		public int powerUpChance = 0;
		public string background = string.Empty;
		public string additionalPrefab = string.Empty;

		public int brickCount = 0;	// All bricks count
		public int destructibleBrickCount = 0;	// Only destructibles

		public char[,,] structure = new char[COLS, ROWS, FLOORS];

		// Constant aliases
		const int ROWS = Constants.Level.Rows;
		const int COLS = Constants.Level.Cols;
		const int FLOORS = Constants.Level.Floors;

		#endregion

		#region Public methods

		public void Reset()
		{
			id = string.Empty;
			powerUpChance = 0;
			background = string.Empty;
			additionalPrefab = string.Empty;
			brickCount = 0;
			destructibleBrickCount = 0;

			structure = new char[COLS, ROWS, FLOORS]; // Reinit = delete
		}

		public void CopyFrom(LevelData source)
		{
			if (source == null)
				return;

			Reset();

			id = source.id;
			powerUpChance = source.powerUpChance;
			background = source.background;
			additionalPrefab = source.additionalPrefab;
			brickCount = source.brickCount;
			destructibleBrickCount = source.destructibleBrickCount;

			for (int floor = 0; floor < FLOORS; floor++)
				for (int col = 0; col < COLS; col++)
					for (int row = 0; row < ROWS; row++)
						structure[col, row, floor] = source.structure[col, row, floor];
		}

		#endregion
	}
}