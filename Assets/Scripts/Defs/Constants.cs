﻿using System.Collections.Generic;

namespace Konyisoft
{
	// Some aliases
	using PowerUpType = PowerUpManager.PowerUpType;
	using SoundSetting = Options.SoundSetting;

	public static class Constants
	{
		public static class Game
		{
			public const int MaxLives = 5;
			public const float EffectsDuration = 15f;  // PowerUp effects duration in seconds
			public const int BombAdditionalScore = 50; // Additional score for a brick when destroyed by a bomb
		}

		public static class Level
		{
			public const int Cols   = 8;    // x axis
			public const int Rows   = 16;   // z axis
			public const int Floors = 6;    // y axis
			
			public const char Empty  = '.';
			public const char Holder = '=';
			
			public static Dictionary<char, string> brickPrefabs = new Dictionary<char, string>()
			{
				// General bricks and ramps
				{ 'r', "Brick (Red)" },
				{ '[', "Brick (Red Triangle Left)" },
				{ ']', "Brick (Red Triangle Right)" },

				{ 'g', "Brick (Green)" },
				{ '{', "Brick (Green Triangle Left)" },
				{ '}', "Brick (Green Triangle Right)" },

				{ 'b', "Brick (Blue)" },
				{ '(', "Brick (Blue Triangle Left)" },
				{ ')', "Brick (Blue Triangle Right)" },

				{ 'y', "Brick (Yellow)" },
				{ '<', "Brick (Yellow Triangle Left)" },
				{ '>', "Brick (Yellow Triangle Right)" },
				
				{ 'o', "Brick (Orange)" },
				{ ';', "Brick (Orange Triangle Left)" },
				{ ':', "Brick (Orange Triangle Right)" },

				{ 'p', "Brick (Purple)" },
				{ '+', "Brick (Purple Triangle Left)" },
				{ '-', "Brick (Purple Triangle Right)" },

				{ 'w', "Brick (White)" },
				{ '!', "Brick (White Triangle Left)" },
				{ '?', "Brick (White Triangle Right)" },
				
				// Special bricks
				{ '2', "Brick (Blue Multi 2)" },
				{ '3', "Brick (Red Multi 3)" },
				{ '4', "Brick (Black Multi 4)" },
				{ 'G', "Brick (Cylinder Gold)" },
				{ 'S', "Brick (Cylinder Silver)" },
				{ 'P', "Brick (Prism)" },
				{ 'H', "Brick (Hit Behind)" },

				// Indestructible
				{ 'B', "Brick (Barrel)" },
				{ 'C', "Brick (Crate 1)" },
				{ 'D', "Brick (Crate 2)" },
				{ 'E', "Brick (Crate 3)" },
				
				// Misc items
				{ '@', "Bomb"},
			};
		}
		
		public static class Scenes
		{
			public const string Title         = "Title";
			public const string Levels        = "Levels";
			public const string Game          = "Game";
			public const string GameOver      = "GameOver";
			public const string Highscore     = "Highscore";
		}
		
		public static class Tags
		{
			public const string InvisibleWall = "InvisibleWall";
			public const string BackWall      = "BackWall";
			public const string Paddle        = "Paddle";
			public const string Ball          = "Ball";
			public const string Brick         = "Brick";
			public const string Effect        = "Effect";
			public const string PowerUp       = "PowerUp";
			public const string SimpleWall    = "SimpleWall";
		}
		
		public static class Layers
		{
			public const int Environment = 8;
			public const int Actors      = 9;
			public const int Effects     = 10;
			public const int Background  = 11;
		}
		
		public static class LayerMasks
		{
			public const int Environment = 1 << Layers.Environment;
			public const int Actors      = 1 << Layers.Actors;
			public const int Effects     = 1 << Layers.Effects;
			public const int Background  = 1 << Layers.Background;
		}
		
		public static class Texts
		{
			// Title
			public const string Reset = "Reset";
			public const string AreYouSure = "Are You Sure?";
			
			// Levels
			public const string LevelNum    = "LEVEL {0}";
			public const string LevelPlay   = "PLAY";
			public const string LevelLocked = "LOCKED";
			
			// Paddle
			public const string Launch    = "launch";
			public const string TryAgain  = "try again";
			public const string Restart   = "restart";
			public const string Continue  = "proceed";
			
			// Large texts
			public const string Level     = "level {0}";
			public const string GameOver  = "game over";
			public const string Failed    = "failed";
			
			// Random 'level done' texts
			public static List<string> LevelDone = new List<string>()
			{
				"impressive",
				"excellent",
				"perfect",
				"delicious",
				"superb",
				"classic",
				"outstanding",
				"awesome"
			};
			
			// Texts shown when hitting a powerup
			public static Dictionary<PowerUpType, string> PowerUps = new Dictionary<PowerUpType, string>()
			{
				{ PowerUpType.AdditionalBall, "multiball" },
				{ PowerUpType.SmallPaddle,    "shrink paddle" },
				{ PowerUpType.LargePaddle,    "expand paddle" },
				{ PowerUpType.StickingBall,   "sticking ball" },
				{ PowerUpType.SpeedUp,        "fast ball" },
				{ PowerUpType.SpeedDown,      "slow ball" },
				{ PowerUpType.InverseControl, "inverted control" },
				{ PowerUpType.ExtraLife,      "extra life!!!" },
				{ PowerUpType.Score500,       "+500" },
				{ PowerUpType.Score1000,      "+1000" },
				{ PowerUpType.Score250,       "+250" },
			};
			
			// Sound settings on title screen
			public static Dictionary<SoundSetting, string> SoundSettings = new Dictionary<SoundSetting, string>()
			{
				{ SoundSetting.MusicAndSound, "Music and SoundFX" },
				{ SoundSetting.MusicOnly,     "Music only" },
				{ SoundSetting.SoundOnly,     "SoundFX only" },
				{ SoundSetting.None,          "Shhhhh!" },
			};
			
		}
	}
}