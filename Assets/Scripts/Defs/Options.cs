﻿using UnityEngine;

namespace Konyisoft
{
	public static class Options
	{
		#region Enumerations
		
		public enum SoundSetting
		{
			MusicAndSound = 1,
			MusicOnly     = 2,
			SoundOnly     = 3,
			None          = 4,
		}
		
		#endregion
		
		#region Fields and properties

		public static bool Loaded
		{
			get { return loaded; }
		}

		public static SoundSetting soundSetting = SoundSetting.MusicAndSound;
		
		static bool loaded = false;

		#endregion

		#region Public methods

		public static void Load()
		{
			soundSetting = (SoundSetting)PlayerPrefs.GetInt("SoundSetting", 1);
			loaded = true;
		}

		public static void Save()
		{
			PlayerPrefs.SetInt("SoundSetting", (int)soundSetting);
		}

		#endregion
	}
}
