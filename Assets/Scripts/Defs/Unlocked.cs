﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Konyisoft
{
	public static class Unlocked
	{
		#region Fields and properties

		public static List<string> Levels
		{
			get { return levels; }
		}

		public static bool Loaded
		{
			get { return loaded; }
		}
		
		static List<string> levels = new List<string>();
		static bool loaded = false;
		
		#endregion

		#region Public methods

		public static void Load()
		{
			levels.Clear();
			
			// Load unlocked level IDs
			if (PlayerPrefs.HasKey("Unlocked"))
			{
				levels = StringToList(PlayerPrefs.GetString("Unlocked"));
			}
			loaded = true;
		}

		public static void Save()
		{
			PlayerPrefs.SetString("Unlocked", ListToString(levels));
			PlayerPrefs.Save();
		}
		
		public static void AddLevel(string id, bool autosave = true)
		{
			if (!levels.Contains(id))
			{
				levels.Add(id);
				if (autosave)
					Save();
			}
		}
		
		public static bool IsLevelUnlocked(string id)
		{
			return levels.Contains(id);
		}

		#endregion
		
		#region Private methods
		
		static string ListToString(List<string> levels)
		{
			string s = string.Empty;
			for (int i = 0; i < levels.Count; i++)
			{
				s += levels[i] + "|";
			}
			s = s.TrimEnd('|'); // Remove the last
			return s;
		}
		
		static List<string> StringToList(string s)
		{
			List<string> levels = new List<string>();
			string[] rows = s.Split('|');
			for (int i = 0; i < rows.Length; i++)
			{
				string id = rows[i];
				if (!string.IsNullOrEmpty(id))
					levels.Add(id);
			}
			return levels;
		}
		
		#endregion
	}
}
