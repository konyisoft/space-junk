﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Konyisoft
{
	public static class Highscore
	{
		#region Classes and structs
		
		public class Score
		{
			public string name = string.Empty;
			public int value = 0;
		}
		
		#endregion

		#region Fields and properties

		public static List<Score> Scores
		{
			get { return scores; }
		}

		public static bool Loaded
		{
			get { return loaded; }
		}
		
		static List<Score> scores = new List<Score>();
		static bool loaded = false;
		
		public const int MaxScores = 10;
		public const int MaxLetters = 12;
		
		#endregion

		#region Public methods

		public static void Load()
		{
			// Load scores
			if (PlayerPrefs.HasKey("Highscore"))
			{
				scores = StringToList(PlayerPrefs.GetString("Highscore"));
			}
			// Save the empty list
			else
			{
				scores = GetDefault();
				Save();
			}
			loaded = true;
		}

		public static void Save()
		{
			scores = Sort(scores);
			PlayerPrefs.SetString("Highscore", ListToString(scores));
			PlayerPrefs.Save();
		}
		
		public static void SaveDefault()
		{
			scores = GetDefault();
			Save();
		}
		
		public static bool AddScore(string name, int value, bool autosave = true)
		{
			return AddScore(new Score()
			{
				name = name,
				value = value
			}, autosave);
		}
		
		public static bool AddScore(Score score, bool autosave = true)
		{
			// NOTE: returning value is true if the new score was added to the list
			Score last = scores.Last();
			// If score value is bigger than the last element's
			if (score.value > last.value)
			{
				scores.Add(score);  // Add the new score
				scores = Sort(scores); // Sort the whole list
				scores.RemoveAt(scores.Count - 1); // Remove the last element
				if (autosave)
					Save();
				return true;
			}
			return false;
		}
		
		public static int GetPosition(int value)
		{
			int index = -1; // Default value if no position achieved
			for (int i = scores.Count - 1; i >= 0; i--)  // Backward
			{
				if (value > scores[i].value)
					index = i;
				else
					break;
			}
			return index;
		}

		#endregion
		
		#region Private methods
		
		static List<Score> GetDefault()
		{
			List<Score> scores = new List<Score>();
			for (int i = 0; i < MaxScores; i++)
			{
				scores.Add(new Score() {
					name = "Konyisoft",
					value = (i + 1) * 100
				});
			}
			return scores;
		}
		
		static List<Score> Sort(List<Score> scores)
		{
			// NOTE: In descending order!
			return scores.OrderByDescending(score => score.value).ToList();
		}
		
		static string ListToString(List<Score> scores)
		{
			string s = string.Empty;
			for (int i = 0; i < scores.Count; i++)
			{
				Score score = scores[i];
				s += score.name + "~" + score.value.ToString() + "|";
			}
			s = s.TrimEnd('|'); // Remove the last
			return s;
		}
		
		static List<Score> StringToList(string s)
		{
			List<Score> scores = GetDefault(); // Default values
			string[] rows = s.Split('|');
			
			for (int i = 0; i < rows.Length; i++)
			{
				// No more than MaxScores
				if (i == MaxScores)
					break;
				
				try
				{
					string[] scoreString = rows[i].Split('~');
					if (scoreString.Length == 2)
					{
						scores[i] = new Score()
						{
							name = scoreString[0],
							value = int.Parse(scoreString[1])
						};
					}
				}
				catch
				{
					// Nothing to do in this case
				}
			}
			return Sort(scores);
		}
		
		#endregion
	}
}
