﻿namespace Konyisoft
{
	// Globally stored values
	public static class Globals
	{
		// The latest score achieved
		public static int lastScore = 0;  
		
		// The name typed in the highscore screen
		public static string lastName = string.Empty;
		
		// The last position on the highscore list
		public static int lastPosition = -1;

		// Show title screen in this mode next time:
		public static TitleManager.Mode titleMode = TitleManager.Mode.Title;
		
		// Show game over screen in this mode next time:
		public static GameOverManager.Mode gameOverMode = GameOverManager.Mode.GameOver;
		
		// The level to start after loading Game scene
		public static int startLevelIndex = 0; // TODO: Set 0 for build!
		
		// Help icons have displayed yet?
		public static bool helpIconsShown = false;
		
		public static void LoadPlayerPrefs()
		{
			if (!Options.Loaded)
				Options.Load();
			if (!Highscore.Loaded)
				Highscore.Load();
			if (!Unlocked.Loaded)
				Unlocked.Load();
		}
	}
}
