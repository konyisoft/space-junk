﻿using UnityEngine;

namespace Konyisoft
{
    public class PulseLaser : GameBehaviour
    {
		#region Fields and properties
		
		public float pulsationSpeed = 1f;
		public bool activeAtStart = true;
		
		Transform beginPoint;
        Transform endPoint;
		Material copyMaterial;
		
		#endregion
		
		#region Mono methods
		
		protected override void Awake()
        {
            base.Awake();

			if (HasMaterial)
			{
				copyMaterial = new Material(Renderer.material);
				Renderer.material = copyMaterial;
			}
			
			beginPoint = Transform.Find("BeginPoint");
			endPoint = Transform.Find("EndPoint");
			
			SetupLineRenderer();
			
			Deactivate();
			if (activeAtStart)
				Activate();
        }
		
		void OnDestroy()
		{
			if (copyMaterial != null)
				Destroy(copyMaterial);
		}	
		
		void Update()
		{
			if (copyMaterial != null)
			{
				Vector2 offset = copyMaterial.GetTextureOffset("_MainTex");
				offset.x += Time.deltaTime * pulsationSpeed;
				copyMaterial.SetTextureOffset("_MainTex", offset);
			}
		}

		#endregion		
		
		#region Public methods

		public void Activate()
		{
			gameObject.SetActive(true);
		}

		public void Deactivate()
		{
			gameObject.SetActive(false);
		}
		
		#endregion
		
		#region Private methods
		
		void SetupLineRenderer()
		{
			if (HasRenderer && beginPoint != null && endPoint != null)
			{
				LineRenderer lineRenderer = Renderer as LineRenderer;
				lineRenderer.positionCount = 2;
				lineRenderer.SetPosition(0, beginPoint.position);
				lineRenderer.SetPosition(1, endPoint.position);
			}
		}
		
		#endregion
	}
}
