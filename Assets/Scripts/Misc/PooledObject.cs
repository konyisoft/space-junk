namespace Konyisoft
{
	public class PooledObject : GameBehaviour
	{
		public ObjectPool Pool { get; set; }
		
		[System.NonSerialized]
		ObjectPool poolInstanceForPrefab;
		
		public void ReturnToPool()
		{
			if (Pool != null)
			{
				Pool.AddObject(this);
			}
			else
			{
				Destroy(gameObject);
			}
		}
		
		public T GetPooledInstance<T>() where T : PooledObject
		{
			if (poolInstanceForPrefab == null)
			{
				poolInstanceForPrefab = ObjectPool.GetPool(this);
			}
			return (T)poolInstanceForPrefab.GetObject();
		}
	}
}