﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Konyisoft
{
	public class ScreenFader : Singleton<ScreenFader>
	{
		#region Fields and properties

		public Image image;
		public float duration = 5f;
		public bool activeAtStart = false;

		public delegate void OnScreenFadedIn();
		public delegate void OnScreenFadedOut();

		public bool IsActive { get { return image != null && image.gameObject.activeInHierarchy; } }
		public bool IsFading { get { return isFading; } }
		
		bool isFading = false;
		bool breakable = false;
		Color origColor = Color.black;

		#endregion

		#region Mono menthods

		protected override void Awake()
		{
			base.Awake();
			
			if (activeAtStart)
				Show();
			else
				Hide();
			
			origColor = image.color;
		}

		#endregion

		#region Public methods

		public void Show(float alpha = 1f)
		{
			StopAllCoroutines();
			SetActive(true);
			SetAlpha(alpha);
			isFading = false;
			breakable = false;
		}

		public void Hide()
		{
			StopAllCoroutines();
			SetAlpha(0f);
			SetActive(false);
			isFading = false;
			breakable = false;
		}
		
		public void SetColor(Color color)
		{
			color.a = image.color.a; // Set correct alpha
			image.color = color;
		}
		
		public void ResetColor()
		{
			Color color = origColor;
			color.a = image.color.a; // Set correct alpha
			image.color = color;
		}

		public void FadeIn(OnScreenFadedIn callback = null, float duration = 0, bool breakable = false)
		{
			if (this.breakable || !isFading)
			{
				this.breakable = breakable;
				StopAllCoroutines(); // Stop current if executing
				isFading = true;
				StartCoroutine(CO_FadeIn(callback, duration));
			}
		}

		public void FadeOut(OnScreenFadedOut callback = null, float duration = 0, bool breakable = false)
		{
			if (this.breakable || !isFading)
			{
				this.breakable = breakable;
				StopAllCoroutines(); // Stop current if executing
				isFading = true;
				StartCoroutine(CO_FadeOut(callback, duration));
			}
		}

		#endregion

		#region Private methods

		void SetAlpha(float alpha)
		{
			if (image != null)
			{
				Color c = image.color;
				c.a = alpha;
				image.color = c;
			}
		}

		float GetAlpha()
		{
			return image != null ? image.color.a : 0f;
		}
		
		IEnumerator CO_FadeIn(OnScreenFadedIn callback = null, float duration = 0f)
		{
			if (duration <= 0)
				duration = this.duration;
			SetActive(true);
			yield return new WaitForSeconds(0.15f); // Wait some time
			yield return StartCoroutine(CoroutineUtils.CO_LerpFloat(1f, 0f, duration, (x) => SetAlpha(x))); 
			SetActive(false); // Become disabled after fading in
			isFading = false;
			if (callback != null) // Important to be here at end
				callback();
		}

		IEnumerator CO_FadeOut(OnScreenFadedOut callback = null, float duration = 0f)
		{
			if (duration <= 0)
				duration = this.duration;
			SetActive(true);
			yield return new WaitForSeconds(0.15f); // Wait some time
			yield return StartCoroutine(CoroutineUtils.CO_LerpFloat(0f, 1f, duration, (x) => SetAlpha(x))); 
			isFading = false;
			if (callback != null)
				callback();
			// NOTE: Remains active after fading out
		}
		
		void SetActive(bool active)
		{
			if (image != null)
				image.gameObject.SetActive(active);
		}

		#endregion
	}
}
