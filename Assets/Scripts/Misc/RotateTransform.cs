using UnityEngine;

namespace Konyisoft
{ 
	public class RotateTransform : GameBehaviour
	{
		public Vector3 speed = Vector3.zero;
		Vector3 origSpeed = Vector3.zero;

		protected override void Awake()
		{
			base.Awake();
			origSpeed = speed;
		}
		
		void Update()
		{
			Transform.Rotate(speed * Time.deltaTime);
		}
		
		public void ResetSpeed()
		{
			speed = origSpeed;
		}
	}
}