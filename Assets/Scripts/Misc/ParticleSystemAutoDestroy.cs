using UnityEngine;

namespace Konyisoft
{ 
	public class ParticleSystemAutoDestroy : GameBehaviour 
	{
		ParticleSystem ps;

		protected override void Awake() 
		{
			base.Awake();
			ps = GetComponent<ParticleSystem>();
		}

		void Update() 
		{
			if (ps != null && !ps.IsAlive())
				Destroy(gameObject);
		}
	}
}