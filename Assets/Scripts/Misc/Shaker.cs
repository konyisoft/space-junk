﻿using UnityEngine;
using System.Collections;

namespace Konyisoft
{
	public class Shaker : GameBehaviour
	{
		public enum Mode
		{
			Increasing,
			Decreasing
		}
		
		public float intensity = 0.3f;
		public float decay = 0.002f;
		public Mode mode = Mode.Decreasing;
		public bool restoreOrigPosition = true;

		Vector3 origPosition = Vector3.zero;
		Quaternion origRotation = Quaternion.identity;
		float currentIntensity = 0.05f;
		bool isShaking = false;
		
		protected override void Awake()
		{
			base.Awake();
			origPosition = Transform.localPosition;
			origRotation = Transform.localRotation;
		}
		
		void Update()
		{
			if (isShaking)
			{
				// Shaking
				if (currentIntensity >= 0f)
				{
					Transform.localPosition = origPosition + Random.insideUnitSphere * currentIntensity;
					Transform.localRotation = new Quaternion(
						origRotation.x + Random.Range(-currentIntensity, currentIntensity) * 0.2f,
						origRotation.y + Random.Range(-currentIntensity, currentIntensity) * 0.2f,
						origRotation.z + Random.Range(-currentIntensity, currentIntensity) * 0.2f,
						origRotation.w + Random.Range(-currentIntensity, currentIntensity) * 0.2f
					);
					currentIntensity -= decay * Time.deltaTime * (mode == Mode.Decreasing ? 1f : -1f);
				}
		
				// Stop
				if ((mode == Mode.Decreasing && currentIntensity <= 0f) ||
					(mode == Mode.Increasing && currentIntensity >= intensity))
				{
					if (restoreOrigPosition)
					{
						Transform.localPosition = origPosition;
						Transform.localRotation = origRotation;
					}
					isShaking = false;
				}		
			}
		}
	 
		public void Shake()
		{
			currentIntensity = mode == Mode.Decreasing ? intensity : 0f;
			isShaking = true;
		}
	}
}
