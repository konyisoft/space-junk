using System.Collections;
using UnityEngine;

namespace Konyisoft
{
	public class Blink : GameBehaviour
	{
		#region Fields and properties
		
		public float fadeInTime = 1f;
		public float fadeOutTime = 1f;
		public float upTime = 1f;
		public float downTime = 1f;
		
		Material copyMaterial; 
		bool isBlinking = true;
		
		#endregion
		
		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			
			if (HasMaterial)
			{
				copyMaterial = new Material(Renderer.material);
				Renderer.material = copyMaterial;
			}
			
			SetAlpha(0);
		} 	
		
		void OnEnable()
		{
			SetAlpha(0);
			isBlinking = true;
			StartCoroutine(CO_Blink());
		}

		void OnDisable()
		{
			isBlinking = false;
			StopAllCoroutines();
			SetAlpha(0);
		}

		void OnDestroy()
		{
			StopAllCoroutines();
			if (copyMaterial != null)
				Destroy(copyMaterial);
		}	
		
		#endregion
		
		#region Private methods
		
		void SetAlpha(float alpha)
		{
			if (copyMaterial != null)
			{
				Color color = copyMaterial.color;
				color.a = alpha;
				copyMaterial.color = color;
			}
		}		
		
		IEnumerator CO_Blink()
		{
			while (isBlinking)
			{
				yield return StartCoroutine(CoroutineUtils.CO_LerpFloat(0f, 1f, fadeInTime, (x) => SetAlpha(x)));
				yield return new WaitForSeconds(upTime);
				yield return StartCoroutine(CoroutineUtils.CO_LerpFloat(1f, 0f, fadeOutTime, (x) => SetAlpha(x)));
				yield return new WaitForSeconds(downTime);
			}
			yield return null;
		}
		
		#endregion
	}
}