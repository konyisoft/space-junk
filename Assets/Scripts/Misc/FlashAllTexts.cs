using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Konyisoft
{
	public class FlashAllTexts : GameBehaviour
	{
		#region Classes and structs
		
		class TextData
		{
			public Text text;
			public Color origColor;
		}
		
		#endregion
	
		#region Fields and properties

		public float flashDuration = 0.35f;
		public Color baseColor = Color.white;
		
		float flashTimer = 0f; 
		List<TextData> flashingTexts = new List<TextData>();
	
		#endregion
		
		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			
			// Get all child texts
			List<Text> texts = GetComponentsInChildren<Text>().ToList();
			texts.ForEach(text =>
			{
				flashingTexts.Add(new TextData() { text = text, origColor = text.color });
			});
		}
		
		void Update()
		{
			// Flashing texts
			flashTimer += Time.deltaTime;
			float t = Mathf.PingPong(flashTimer, flashDuration) / flashDuration;
			flashingTexts.ForEach(textData =>
			{
				textData.text.color = Color.Lerp(baseColor, textData.origColor, t);
			});
		}

		#endregion
	}
}
