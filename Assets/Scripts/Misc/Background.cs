using UnityEngine;

namespace Konyisoft
{
	public class Background : GameBehaviour
	{
		#region Fields and properties
		
		Material copyMaterial; 
		
		#endregion
		
		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			
			if (HasMaterial)
			{
				copyMaterial = new Material(Renderer.material);
				Renderer.material = copyMaterial;
			}
		} 	

		void OnDestroy()
		{
			if (copyMaterial != null)
				Destroy(copyMaterial);
		}	
		
		#endregion
		
		#region Public methods
		
		public void LoadTexture(string prefabName)
		{
			if (copyMaterial != null)
				copyMaterial.mainTexture = ResourceManager.Instance.GetTexture2D(prefabName);
		}
		
		#endregion
	}
}