﻿using UnityEngine;
using Konyisoft;

namespace Konyisoft
{
	public class CombineMeshToCollider : MonoBehaviour
	{
		#region Fields and properties
		
		public bool generateAtAwake = false;
		
		#endregion

		#region Mono method
		
		void Awake()
		{
			if (generateAtAwake)
				GenerateCollider();
		}
		
		#endregion

		#region Public methods
		
		public void GenerateCollider()
		{
			MeshCollider meshCollider = GetComponent<MeshCollider>();
			if (meshCollider == null)
				meshCollider = gameObject.AddComponent<MeshCollider>();
			
			meshCollider.sharedMesh = null;
			MeshFilter[] meshFilters = GetComponentsInChildren<MeshFilter>();
			CombineInstance[] combine = new CombineInstance[meshFilters.Length];
			int i = 0;
			while (i < meshFilters.Length)
			{
				// Create combined mesh from renderers
				Renderer renderer = meshFilters[i].GetComponent<Renderer>();
				if (renderer != null && renderer.enabled)
				{
					combine[i].mesh = meshFilters[i].sharedMesh;
					combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
				}
				
				// Remove original colliders
				Collider collider = meshFilters[i].GetComponent<Collider>();
				if (collider != null)
					Destroy(collider);
				
				i++;
			}
			Mesh mesh = new Mesh();
			mesh.CombineMeshes(combine);
			meshCollider.sharedMesh = mesh;
		}
		
		#endregion
	}
}
