using System.Collections;
using UnityEngine;

namespace Konyisoft
{
	[System.Serializable]
	public struct FloatRange
	{
		public float min, max;
		
		public float RandomInRange
		{
			get { return Random.Range(min, max); }
		}
	}	
	
	public class TitleBrickSpawner : GameBehaviour
	{
		#region Fields and properties

		public TitleBrick[] brickPrefabs;
		public Material[] brickMaterials;
		public float spawnRadius = 10f;
		public float velocity = 10f;
		public FloatRange timeBetweenSpawns;
		public FloatRange scale;
		public FloatRange randomVelocity;
		public FloatRange angularVelocity;
		
		float timeSinceLastSpawn = 0f;
		float currentSpawnDelay = 0f;
		
		#endregion
		
		#region Mono methods

		void FixedUpdate()
		{
			timeSinceLastSpawn += Time.deltaTime;
			if (timeSinceLastSpawn >= currentSpawnDelay)
			{
				timeSinceLastSpawn -= currentSpawnDelay;
				currentSpawnDelay = timeBetweenSpawns.RandomInRange;
				SpawnBrick();
			}		
		}

		#endregion
		
		#region Private methods

		void SpawnBrick()
		{
			if (brickPrefabs.Length > 0)
			{
				TitleBrick brickPrefab = brickPrefabs[Random.Range(0, brickPrefabs.Length)];
				if (brickPrefab != null)
				{
					TitleBrick brick = brickPrefab.GetPooledInstance<TitleBrick>();

					// Transform settings
					Vector3 spawnPoint = Random.insideUnitSphere * spawnRadius;
					spawnPoint.z = 0f;
					brick.Transform.SetParent(Transform);
					brick.Transform.localScale = brick.OrigScale * scale.RandomInRange;
					brick.Transform.localRotation = Random.rotation;
					brick.Transform.localPosition = spawnPoint;
				
					// Rigidbody settings
					brick.Rigidbody.velocity = -transform.forward * velocity + Random.onUnitSphere * randomVelocity.RandomInRange;
					brick.Rigidbody.angularVelocity = Random.onUnitSphere * angularVelocity.RandomInRange;					
					
					brick.Renderer.material =  brickMaterials[Random.Range(0, brickMaterials.Length)];
				}
			}
		}
		
		#endregion
	}
}