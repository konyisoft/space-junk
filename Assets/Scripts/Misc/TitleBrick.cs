using System.Collections;
using UnityEngine;

namespace Konyisoft
{
	public class TitleBrick : PooledObject
	{
		#region Fileds and properties
		
		public Vector3 OrigScale { get; private set; }
		
		#endregion
		
		#region Mono methods
		
		protected override void Awake()
		{
			base.Awake();
			OrigScale = Transform.localScale;
		}
		
		#endregion
		
		#region Event handlers

		void OnTriggerEnter (Collider enteredCollider)
		{
			if (enteredCollider.CompareTag("BackWall"))
			{
				ReturnToPool();
			}
		}
		
		#endregion
	}
}