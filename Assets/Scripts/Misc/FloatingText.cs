﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Konyisoft
{
    public class FloatingText : GameBehaviour
    {
		#region Fields and properties
		
		public float Alpha 
		{
			get { return GetAlpha(); }
		}
		
		public string Text
		{
			get { return GetText(); }
			set { SetText(value); }
		}
		
		public float fadeInTime = 0.75f;
		public float fadeOutTime = 0.75f;
		public float speed = 5f;
		
		Text text;
		
		#endregion
		
		#region Mono methods
		
		protected override void Awake()
        {
            base.Awake();
			text = GetComponent<Text>();
			SetAlpha(0f);
        }
		
		void Start()
		{
			StartCoroutine(CO_Fade());
		}
		
		void OnDestroy()
		{
			StopAllCoroutines();
		}	
		
		void Update()
		{
			if (text != null)
				transform.Translate(Vector3.up * Time.deltaTime * speed);
		}

		#endregion
		
		#region Private methods

		void SetAlpha(float alpha)
		{
			if (text != null)
			{
				Color color = text.color;
				color.a = alpha;
				text.color = color;
			}
		}
		
		float GetAlpha()
		{
			return text != null ? text.color.a : 0f;
		}
		
		void SetText(string text)
		{
			if (text != null)
				this.text.text = text;			
		}
		
		string GetText()
		{
			return text != null ? text.text : string.Empty;
		}
		
		IEnumerator CO_Fade()
		{
			yield return StartCoroutine(CoroutineUtils.CO_LerpFloat(0f, 1f, fadeInTime, (x) => SetAlpha(x)));
			yield return StartCoroutine(CoroutineUtils.CO_LerpFloat(1f, 0f, fadeOutTime, (x) => SetAlpha(x)));
			Destroy(gameObject);  // Auto destroy
		}
		
		#endregion
	}
}
