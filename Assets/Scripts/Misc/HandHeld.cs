﻿using UnityEngine;
using System.Collections;

namespace Konyisoft
{
	// NOTE: for UI RectTransform only
	public class HandHeld : GameBehaviour
	{
		#region Fields and properties
		
		public Vector3 range = new Vector3(0.7f, 0.7f, 0f);
		public float speed = 1f;

		Vector3 origPosition = Vector3.zero;
		Vector3 desiredPosition = Vector3.zero;
		
		RectTransform rectTransform;
		
		#endregion
		
		#region Mono methods
		
		protected override void Awake()
		{
			base.Awake();
			rectTransform = Transform as RectTransform;
		}
		
		void Start()
		{
			origPosition = rectTransform.localPosition;
			SetDesiredPosition();
		}
		
		void Update()
		{
			rectTransform.localPosition = Vector3.MoveTowards(rectTransform.localPosition, desiredPosition, Time.deltaTime * speed);
			
			if (Vector3.Distance(rectTransform.localPosition, desiredPosition) < 0.01f)
			{
				rectTransform.localPosition = desiredPosition;
				SetDesiredPosition();
			}
		}
		
		#endregion
		
		#region Private methods
		
		void SetDesiredPosition()
		{
			Vector3 randomPosition = new Vector3(
				Random.Range(-range.x, range.x),
				Random.Range(-range.y, range.y),
				Random.Range(-range.z, range.z)
			);
			desiredPosition = origPosition + randomPosition;
		}
		
		#endregion
	}
}