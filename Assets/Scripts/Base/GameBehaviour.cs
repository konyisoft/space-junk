﻿using UnityEngine;

namespace Konyisoft
{
	public class GameBehaviour : MonoBehaviour
	{
		public Renderer Renderer { get { return _renderer; } }
		public Rigidbody Rigidbody { get { return _rigidbody; }	}
		public Transform Transform { get { return _transform; }	}
		public Collider Collider { get { return _collider; }	}

		public bool HasRenderer { get { return _renderer != null; } }
		public bool HasMaterial { get { return HasRenderer && _renderer.material != null; } }
		public bool HasRigidbody { get { return _rigidbody != null; } }
		public bool HasTransform { get { return _transform != null; } }
		public bool HasCollider { get { return _collider != null; } }

		Renderer _renderer;
		Rigidbody _rigidbody;
		Transform _transform;
		Collider _collider;

		protected virtual void Awake()
		{
			_renderer = GetComponent<Renderer>();
			_rigidbody = GetComponent<Rigidbody>();
			_transform = GetComponent<Transform>();
			_collider = GetComponent<Collider>();
		}
	}
}
