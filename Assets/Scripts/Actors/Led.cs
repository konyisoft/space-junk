using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Konyisoft
{
	public class Led : GameBehaviour
	{
		#region Fields and properties
		
		public bool IsOn
		{
			get { return isOn; }
		}

		public bool IsBlinking
		{
			get { return isBlinking; }
		}

		public Color color = Color.white;
		public PowerUpManager.PowerUpType powerUpType = PowerUpManager.PowerUpType.Unset;
		public float blinkDuration = 0.33f;

		Image image;
		Color origColor = Color.white;
		bool isOn = false;
		bool isBlinking = false;
		
		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();

			image = GetComponent<Image>();
			if (image != null)
				origColor = image.color;
		
			TurnOff();
		}
		
		void OnDestroy()
		{
			StopAllCoroutines();
		}	
		
		#endregion
		
		#region Public methods

		public void TurnOn()
		{
			StopAllCoroutines();
			if (image != null)
				image.color = color;
			isOn = true;
			isBlinking = false;
		}
		
		public void Blink()
		{
			if (!isBlinking)
			{
				isBlinking = true;
				StartCoroutine(CO_Blink());
			}
		}
		
		public void TurnOff()
		{
			StopAllCoroutines();
			if (image != null)
				image.color = origColor;
			isOn = false;
			isBlinking = false;
		}
		
		#endregion
		
		#region Private methods

		IEnumerator CO_Blink()
		{
			while (isBlinking && image != null)
			{
				image.color = origColor;
				yield return new WaitForSeconds(blinkDuration);
				image.color = color;
				yield return new WaitForSeconds(blinkDuration);
			}
			yield return null;
		}
		
		#endregion
	}
}