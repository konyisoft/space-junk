using UnityEngine;
using System.Collections;

namespace Konyisoft
{
	public class Ball : GameBehaviour
	{
		#region Enumerations
		
		public enum Direction
		{
			Forward = 1,
			Backward = -1
		}
		
		#endregion

		#region Fields and properties
		
		public bool IsPlaying
		{
			get { return isPlaying; }
		}
		
		public float CurrentSpeed
		{
			get { return currentSpeed; }
			set { currentSpeed = value; }
		}
		
		public float normalSpeed = 5f;
		public float slowSpeed = 5f;
		public float fastSpeed = 5f;
		public bool randomDirectionAtStart = true;
		public Direction startDirection = Direction.Forward;
		public float flyAwaySpeed = 5f;

		float currentSpeed = 5f;
		bool isPlaying = false;
		bool isFlyingAway = false;
		bool requestMinimalVelocityChange = false;
		
		const float RANDOMFORCE = 0.8f;

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			NormalSpeed();
		}
		
		void Update()
		{
			if (isFlyingAway)
			{
				Vector3 position = Transform.position;
				position.y += Time.deltaTime * flyAwaySpeed;
				Transform.position = position;
			}
		}
		
		void FixedUpdate()
		{
			if (isPlaying)
			{
				// Add some random x or z velocity if would be too small
				// (Prevent from stucking in one axis)

				Vector3 velocity = Rigidbody.velocity;

				// On x axis
				if (Mathf.Abs(velocity.x - 0.35f) <= 0.35f)
				{
					if (velocity.x < 0f)
						velocity.x -= RANDOMFORCE;
					else if (velocity.x > 0f)
						velocity.x += RANDOMFORCE;
					else
						velocity.x = Random.Range(0, 2) == 0 ? RANDOMFORCE : -RANDOMFORCE;
				}
				
				// On Z axis
				if (Mathf.Abs(velocity.z - 0.35f) <= 0.35f)
				{
					if (velocity.z < 0f)
						velocity.z -= RANDOMFORCE;
					else if (velocity.z > 0f)
						velocity.z += RANDOMFORCE;
					else
						velocity.z = Random.Range(0, 2) == 0 ? RANDOMFORCE : -RANDOMFORCE;
				}
				
				// After hitting a brick we apply a minimal velocity change
				if (requestMinimalVelocityChange)
				{
					float randomX = Random.Range(0.02f, 0.05f);
					float randomZ = Random.Range(0.02f, 0.05f);
					velocity.x += velocity.x > 0 ? randomX : -randomX;
					velocity.z += velocity.z > 0 ? randomZ : -randomZ;
					requestMinimalVelocityChange = false;
				}
				
				Rigidbody.velocity = velocity.normalized * currentSpeed;
			}
		}		

		void OnCollisionEnter(Collision collision)
		{
			// Brick? Add a very small random force (to prevent physics stucking)
			if (collision.gameObject.CompareTag(Constants.Tags.Brick))
			{
				requestMinimalVelocityChange = true;
			}
			// Hit the paddle?
			else if (collision.gameObject.CompareTag(Constants.Tags.Paddle))
			{
				// Can ball stick?
				if (GameManager.Instance.CanBallStick(this))
				{
					AudioManager.Instance.PlaySound("Stick Ball");
					GameManager.Instance.StickBall(this);
				}
				else
				{
					float currentSpeed = Rigidbody.velocity.magnitude;
					Vector3 currentDirection = Rigidbody.velocity.normalized;
					
					// Calculate hit factor (double)
					float xModifier = HitFactor(transform.position, collision.transform.position, collision.collider.bounds.size.x) * 2.2f;
					
					// Calculate new direction, set length to 1
					bool forward = Rigidbody.velocity.z > 0f;
					Vector3 newDirection = new Vector3(currentDirection.x + xModifier, 0f, forward ? 1f : -1f).normalized;
					
					// Set velocity with direction * speed
					Rigidbody.velocity = newDirection * currentSpeed;
					
					// Keep the speed constant (simple method)
					//Rigidbody.velocity = Rigidbody.velocity.normalized * currentSpeed;
					
					// Punch! Only in playing mode
					if (GameManager.Instance.State == GameManager.GameState.Playing)
						iTween.PunchScale(GameManager.Instance.paddle.gameObject, new Vector3(0.12f, 0f, 0.12f), 0.3f);
					
					AudioManager.Instance.PlaySound("Paddle Impact");
				}
			}
			else if (collision.gameObject.CompareTag(Constants.Tags.InvisibleWall))
			{
				AudioManager.Instance.PlaySound("Invisible Wall Impact");
				EffectManager.Instance.InstantiateRipples(collision.contacts[0].point);
			}
			else if (collision.gameObject.CompareTag(Constants.Tags.SimpleWall))
			{
				AudioManager.Instance.PlaySound("Simple Wall Impact");
			}
		}

		void OnTriggerEnter(Collider other)
		{
			if (other.gameObject.CompareTag(Constants.Tags.BackWall))
			{
				GameManager.Instance.BallOutOfField(this);
				StartCoroutine(CO_Terminate(1.5f));
			}
		}
		
		void OnDestroy()
		{
			StopAllCoroutines();
			GameManager.Instance.RemoveBall(this);
		}
		
		#endregion
		
		#region Public methods

		public void Play()
		{
			StopAllCoroutines();
			gameObject.SetActive(true);
			isPlaying = true;
			isFlyingAway = false;
			requestMinimalVelocityChange = false;
			Rigidbody.isKinematic = false;
			//Collider.enabled = true;  // NOTE: need this?
			
			Vector3 direction = randomDirectionAtStart ? 
				new Vector3(Random.Range(-1f, 1f), 0, (int)startDirection).normalized :
				new Vector3(0, 0, (int)startDirection);

			Rigidbody.velocity = direction * currentSpeed;
		}
		
		public void Stop()
		{
			StopAllCoroutines();
			gameObject.SetActive(true);
			isPlaying = false;
			isFlyingAway = false;
			requestMinimalVelocityChange = false;
			Rigidbody.isKinematic = true;
			Rigidbody.velocity = Vector3.zero;
			//Collider.enabled = false;  // NOTE: need this?
		}
		
		public void Terminate()
		{
			Destroy(gameObject);
		}
		
		public void FlyAway()  // ...and destroy when the current level has done 
		{
			Stop();
			isFlyingAway = true;
			StartCoroutine(CO_Terminate(2.5f));
		}
		
		public void SpeedUp()
		{
			currentSpeed = fastSpeed;
		}

		public void SpeedDown()
		{
			currentSpeed = slowSpeed;
		}

		public void NormalSpeed()
		{
			currentSpeed = normalSpeed;
		}
		
		public void Teleport(Vector3 position, Vector3 direction)
		{
			Rigidbody.position = position;
			Rigidbody.velocity = direction * currentSpeed;
		}
		
		public void RandomDirection()
		{
			Vector2 randomPoint = Random.insideUnitCircle.normalized;
			Rigidbody.velocity = new Vector3(randomPoint.x, 0f, randomPoint.y) * currentSpeed;
		}

		#endregion
		
		#region Private methods

		float HitFactor(Vector3 ballPos, Vector3 paddlePos, float paddleWidth)
		{
			// ascii art:
			//
			// 1  -0.5  0  0.5   1  <- x value
			// ===================  <- paddle
			//
			return (ballPos.x - paddlePos.x) / paddleWidth;
		}
		
		IEnumerator CO_Terminate(float delay)
		{
			yield return new WaitForSeconds(delay);
			Terminate();
		}
		
		#endregion
	}
}