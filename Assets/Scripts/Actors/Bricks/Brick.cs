using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Konyisoft
{
	public class Brick : GameBehaviour
	{
		#region Classes and structs
		
		[System.Serializable]
		public class PowerUpDrop
		{
			public PowerUpManager.PowerUpType type = PowerUpManager.PowerUpType.Unset;
			public int weight = 1;
		}
		
		#endregion

		#region Fields and properties
		
		public int Col
		{
			get { return col; }
		}

		public int Row
		{
			get { return row; }
		}
		
		public int Floor
		{
			get { return floor; }
		}

		public bool HasPowerUp
		{
			get { return powerUpDrops.Count > 0; }
		}
		
		public float fallSpeed = 2f;
		public int score = 100;
		public bool indestructible = false;
		public bool destroyableWithBomb = false;
		public bool alwaysDropPowerUp = false;
		public List<PowerUpDrop> powerUpDrops = new List<PowerUpDrop>();
		public string hitAudioID = string.Empty;
		public string destroyAudioID = string.Empty;

		Vector3 desiredPosition;
		Vector3 nextPosition;
		bool nextPositionWasSet = false;
		bool isFalling = false;

		int col = -1;    // x
		int row = -1;    // z
		int floor = -1;  // y
		
		#endregion

		#region Mono methods

		protected virtual void Update()
		{
			if (isFalling)
			{
				Transform.position = Vector3.MoveTowards(Transform.position, desiredPosition, Time.deltaTime * fallSpeed);
				
				if (Transform.position.y <= desiredPosition.y)
				{
					Transform.position = desiredPosition;
					isFalling = false;
				}
			}
		}
		
		void OnCollisionEnter(Collision collision)
		{
			if (isFalling || floor > 0)
				return;
			
			// Hit by the ball?
			if (collision.gameObject.CompareTag(Constants.Tags.Ball))
			{
				HandleBallCollision(collision);
			}
		}
		
		#endregion
		
		#region Public methods

		public void FallDown()
		{
			// Initial position when falling the first time
			if (!nextPositionWasSet)
			{	
				nextPosition = Transform.position;
				nextPositionWasSet = true;
			}
			
			// Fall down a height of 1 brick
			nextPosition.y -= LevelManager.Instance.brickSize.y;
			desiredPosition = nextPosition;
			isFalling = true;
		}
		
		public void SetCoords(int col, int row, int floor)
		{
			this.col = col;
			this.row = row;
			this.floor = floor;
		}
		
		public virtual void ForceDestroy()
		{
			// Can be called externally to destroy brick
			// Destroys anyway, no collision checking performed
			if (!indestructible)
				GameManager.Instance.DestroyBrick(this);
		}
		
		public PowerUpManager.PowerUpType DropPowerUp()
		{
			PowerUpManager.PowerUpType r = PowerUpManager.PowerUpType.Unset;
			if (powerUpDrops.Count > 0)
			{
				// Create a weighted list from powerup types
				List<PowerUpManager.PowerUpType> weighted = new List<PowerUpManager.PowerUpType>();
				powerUpDrops.ForEach(pud =>
				{
					if (pud.type != PowerUpManager.PowerUpType.Unset && pud.weight > 0)
					{
						for (int i = 0; i < pud.weight; i++)
						{
							weighted.Add(pud.type);
						}
					}
				});
				r = weighted[Random.Range(0, weighted.Count)];
			}
			return r;
		}
		
		#endregion
		
		#region Protected methods

		protected virtual void HandleBallCollision(Collision collision)
		{
			// Overridable by descendants
			if (!indestructible && !destroyableWithBomb)
			{
				if (!string.IsNullOrEmpty(destroyAudioID))
					AudioManager.Instance.PlaySound(destroyAudioID);
				GameManager.Instance.DestroyBrick(this);
			}
			else
			{
				if (!string.IsNullOrEmpty(hitAudioID))
					AudioManager.Instance.PlaySound(hitAudioID);
			}
		}

		#endregion
		
		#region Private methods

		#endregion
	}
}