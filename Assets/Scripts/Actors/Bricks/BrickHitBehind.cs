using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Konyisoft
{
	// A brick can only be destroyed by hitting its back.
	public class BrickHitBehind : Brick
	{
		#region Protected methods

		protected override void HandleBallCollision(Collision collision)
		{	
			// NOTE: Collision = always the ball
			if (collision.contacts.Length > 0)
			{
				// Ball->Brick direction
				Vector3 direction = (Transform.position - collision.transform.position).normalized;
				float forwardDot = Vector3.Dot(direction, Vector3.forward);
				// Hit from behind? Only in this case we call the base method.
				if (forwardDot < -0.25f)
					base.HandleBallCollision(collision);
				else
					AudioManager.Instance.PlaySound("Indestructible Impact 1");
			}
		}

		#endregion
	}
}