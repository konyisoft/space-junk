using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Konyisoft
{
	// A bomb!
	public class BrickBomb : Brick
	{
		#region Public methods
		
		public override void ForceDestroy()
		{
			base.ForceDestroy();
			
			if (!indestructible)
				GameManager.Instance.DetonateBrick(this);
		}
		
		#endregion
		
		
		#region Protected methods

		protected override void HandleBallCollision(Collision collision)
		{	
			base.HandleBallCollision(collision);
			
			if (!indestructible)
				GameManager.Instance.DetonateBrick(this);
		}

		#endregion
	}
}