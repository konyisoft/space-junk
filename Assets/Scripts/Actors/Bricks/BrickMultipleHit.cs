using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Konyisoft
{
	public class BrickMultipleHit : Brick
	{
		#region Fields and properties
		
		public int hitsToDestroy = 1;
		
		Material copyMaterial;
		Color origColor;
		List<TextMesh> numberTexts = new List<TextMesh>();
		
		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			
			if (HasMaterial)
			{
				copyMaterial = new Material(Renderer.material);
				copyMaterial.EnableKeyword ("_EMISSION");
				Renderer.material = copyMaterial;
			}
			
			origColor = copyMaterial.GetColor("_EmissionColor");			
			numberTexts = GetComponentsInChildren<TextMesh>().ToList();
			WriteNumber(hitsToDestroy);
		}
		
		void OnDestroy()
		{
			StopAllCoroutines();
			if (copyMaterial != null)
				Destroy(copyMaterial);
		}

		#endregion
		
		#region Public methods

		#endregion
		
		#region Protected methods

		protected override void HandleBallCollision(Collision collision)
		{
			// Call the base when no more hits
			if (--hitsToDestroy == 0)
			{
				base.HandleBallCollision(collision);
			}
			// Otherwise do a hit blinking
			else
			{
				StopAllCoroutines();
				AudioManager.Instance.PlaySound(hitAudioID);
				WriteNumber(hitsToDestroy);
				StartCoroutine(CO_Blink());
			}
		}

		#endregion
		
		#region Private methods

		void SetEmissiveColor(Color color)
		{
			if (copyMaterial != null)
			{
				copyMaterial.SetColor("_EmissionColor", color);
			}
		}		
		
		IEnumerator CO_Blink()
		{
			if (copyMaterial != null)
			{
				yield return StartCoroutine(CoroutineUtils.CO_LerpColor(origColor, Color.grey, 0.075f, (x) => SetEmissiveColor(x)));
				yield return StartCoroutine(CoroutineUtils.CO_LerpColor(Color.grey, origColor, 0.075f, (x) => SetEmissiveColor(x)));
			}	
			else
				yield return null;
		}
		
		void WriteNumber(int number)
		{
			numberTexts.ForEach(numberText => 
			{
				numberText.text = number.ToString();
			});
		}

		#endregion
	}
}