using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Konyisoft
{
	public class Paddle : GameBehaviour
	{
		#region Fields and properties

		public bool CanMove
		{
			get { return canMove; }
		}
		
		public bool HasBallAttached
		{
			get { return attachedBall != null; }
		}
		
		public Ball AttachedBall
		{
			get { return attachedBall; }
		}
		
		public bool IsInverseControl
		{
			get { return isInverseControl; }
		}
		
		public float paddleSpeed = 1f;
		public float minX = -3.5f;
		public float maxX = 3.5f;
		public float smallSizeScale = 0.5f;
		public float largeSizeScale = 1.5f;
		public float normalSizeScale = 1f;

		bool canMove = false;
		Ball attachedBall = null;
		bool isBallAttaching = false;
		bool isInverseControl = false;
	
		Vector3 origPosition;
		Transform ballSlotTransform;
		TextMesh textMesh;
		float desiredScale = 1f;

		#endregion

		#region Mono methods
		
		protected override void Awake()
		{
			base.Awake();
			origPosition = Transform.position;
			desiredScale = normalSizeScale;
			ballSlotTransform = Transform.Find("Ball Slot");
			textMesh = GetComponentInChildren<TextMesh>();
			SetText(string.Empty);
			SetTextVisible(false);
		}

		void Update()
		{
			// Resizing
			Vector3 localScale = Transform.localScale;
			if (localScale.x != desiredScale)
			{
				localScale.x = Mathf.Lerp(localScale.x, desiredScale, Time.deltaTime * 10f);
				Transform.localScale = localScale;
				Transform.position = ClampPosition(Transform.position, localScale);
			}
			
			// Attach of a sticking ball with Lerp
			if (isBallAttaching && attachedBall != null)
			{
				float distance = Vector3.Distance(attachedBall.Transform.position, ballSlotTransform.position);
				if (distance < 0.1f)
				{
					isBallAttaching = false;
					AlignBallPosition(true);
				}
				else
				{
					AlignBallPosition(false);					
				}
			}
		}
		
		#endregion
		
		#region Public methods
		
		public void Play()
		{
			canMove = true;
		}

		public void Stop()
		{
			canMove = false;
		}

		public void Restore()
		{
			SetText(string.Empty);
			SetTextVisible(false);
			Transform.position = origPosition;
			NormalSize();
			NormalControl();
		}
	
		public void AttachBall(Ball ball, bool immediate)
		{
			if (ball != null)
			{
				attachedBall = ball;
				isBallAttaching = !immediate;
				if (immediate)
					AlignBallPosition(true);
			}
		}

		public void DetachBall()
		{
			attachedBall = null;
			isBallAttaching = false;
		}
		
		public void SetText(string text)
		{
			if (textMesh != null)
				textMesh.text = text;
		}
		
		public void SetTextVisible(bool visible)
		{
			if (textMesh != null)
				textMesh.gameObject.SetActive(visible);
		}
		
		public void SmallSize()
		{
			desiredScale = smallSizeScale;
		}
		
		public void LargeSize()
		{
			desiredScale = largeSizeScale;
		}
		
		public void NormalSize()
		{
			desiredScale = normalSizeScale;
		}
		
		public void InverseControl()
		{
			isInverseControl = true;
		}

		public void NormalControl()
		{
			isInverseControl = false;
		}

		#endregion
		
		#region Internal methods
		
		internal void Move(float horizontal)
		{
			if (isInverseControl)
				horizontal *= -1f;  // Invert horizontal movement
			
			Vector3 position = Transform.position;
			position.x = position.x + (horizontal * paddleSpeed);
			position = ClampPosition(position, Transform.localScale);
			Transform.position = position;
			AlignPerspectiveCameraPosition();
			if (attachedBall != null && !isBallAttaching)
				AlignBallPosition(true);
		}
		
		#endregion
		
		#region Private methods
		
		void AlignBallPosition(bool immediate)
		{
			if (ballSlotTransform != null)
			{
				if (immediate)
					attachedBall.Transform.position = ballSlotTransform.position;
				else
					attachedBall.Transform.position = Vector3.Lerp(attachedBall.Transform.position, ballSlotTransform.position, Time.deltaTime * 35f);
			}
		}
		
		void AlignPerspectiveCameraPosition()
		{
			Camera camera = DisplayManager.Instance.perspectiveCamera;
			if (camera != null && camera.transform.parent != null) // Camera has rig?
			{
				Transform rigTransform = camera.transform.parent;
				Vector3 rigPosition = rigTransform.position;
				rigPosition.x = Transform.position.x / 17f;
				rigTransform.position = rigPosition;
				
				// Rotation test
				rigTransform.localEulerAngles = new Vector3(
					rigTransform.localEulerAngles.x,
					rigTransform.localEulerAngles.y,
					Transform.position.x * 0.25f);  // Simplest way to apply some rotation based on x movement
			}
		}

		Vector3 ClampPosition(Vector3 position, Vector3 localScale)
		{
			// Clamping position between minX and maxX based on current paddle size (localScale.x)
			float modifier = (localScale.x - normalSizeScale) * 0.5f;
			position.x = Mathf.Clamp(position.x, minX + modifier, maxX - modifier);
			return position;
		}
		
		#endregion
	}
}