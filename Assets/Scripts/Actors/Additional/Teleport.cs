﻿using UnityEngine;

namespace Konyisoft
{
	public class Teleport: GameBehaviour
	{
		#region Fields and properties

		TeleportContainer container;
		
		#endregion

		#region Mono methods
		
		protected override void Awake()
		{
			base.Awake();
			container = GetComponentInParent<TeleportContainer>();
		}
		
		void OnTriggerEnter(Collider collider)
		{
			if (container == null || container.LastTarget != null)
				return;

			// Hit by the ball?
			if (collider.gameObject.CompareTag(Constants.Tags.Ball))
			{
				Teleport teleport = container.GetRandomTeleport(this);
				if (teleport != null)
				{
					container.LastTarget = teleport; 
					GameManager.Instance.MainBall.Teleport(teleport.Transform.position, teleport.Transform.forward);
					AudioManager.Instance.PlaySound("Teleport Impact");
				}
			}
		}
		
		void OnTriggerExit(Collider collider)
		{
			if (collider.gameObject.CompareTag(Constants.Tags.Ball) &&
				container.LastTarget == this)
			{
				container.LastTarget = null;
			}

			
		}
		
		#endregion
	}
}