﻿using UnityEngine;

namespace Konyisoft
{
    public class LaserWall : Switchable
    {
		#region Enumerations
		
		private enum State
		{
			Enabled,
			Disabled
		}
		
		#endregion
		
		#region Fields and properties
		
		public float pulsationSpeed = 1f;
		public float minEnabledTime = 3f;
		public float maxEnabledTime = 10f;
		public float minDisabledTime = 3f;
		public float maxDisabledTime = 10f;
		public bool continuous = false;
		
		public string activeAudioID = string.Empty;
		public string hitAudioID = string.Empty;
		
		Material copyMaterial;
		float timer = 0;
		float enabledTime = 0;
		float disabledTime = 0;
		State state = State.Disabled;
		
		#endregion
		
		#region Mono methods
		
		protected override void Awake()
        {
            base.Awake();

			if (HasMaterial)
			{
				copyMaterial = new Material(Renderer.material);
				Renderer.material = copyMaterial;
			}
			
			RandomizeTimes();
			
			if (continuous)
				Enable();
			else
				Disable();
        }
		
		void OnDestroy()
		{
			if (!string.IsNullOrEmpty(activeAudioID))
				AudioManager.Instance.Stop(activeAudioID);
			
			if (copyMaterial != null)
				Destroy(copyMaterial);
		}	
		
		void Update()
		{
			// Animating texture
			if (copyMaterial != null)
			{
				Vector2 offset = copyMaterial.GetTextureOffset("_MainTex");
				offset.x += Time.deltaTime * pulsationSpeed;
				copyMaterial.SetTextureOffset("_MainTex", offset);
			}
			
			// Doesn't count time in continuous mode
			if (continuous)
				return;
			
			// Enable/disable timer. Toggling between states.
			timer += Time.deltaTime;
			if (state == State.Enabled && timer >= enabledTime)
			{
				Disable();
			}
			else if (state == State.Disabled && timer >= disabledTime)
			{
				Enable();
			}
		}

		#endregion		
		
		#region Private methods
		
		void RandomizeTimes()
		{
			enabledTime = Random.Range(minEnabledTime, maxEnabledTime);
			disabledTime = Random.Range(minDisabledTime, maxDisabledTime);
		}
		
		void Enable()
		{
			if (HasCollider)
				Collider.enabled = true;
			if (HasRenderer)
				Renderer.enabled = true;
			state = State.Enabled;
			timer = 0;
			
			if (!string.IsNullOrEmpty(activeAudioID))
				AudioManager.Instance.PlaySound(activeAudioID);
		}
		
		void Disable()
		{
			if (HasCollider)
				Collider.enabled = false;
			if (HasRenderer)
				Renderer.enabled = false;
			state = State.Disabled;
			timer = 0;
			
			if (!string.IsNullOrEmpty(activeAudioID))
				AudioManager.Instance.Stop(activeAudioID);
		}

		#endregion
		
		#region Event handlers
		
		void OnCollisionEnter(Collision collision)
		{
			// Hit by the ball?
			if (collision.gameObject.CompareTag(Constants.Tags.Ball))
			{
				if (!string.IsNullOrEmpty(hitAudioID))
					AudioManager.Instance.PlaySound(hitAudioID);
			}
		}
		
		#endregion
		
		#region ISwitchable
		
		public override void SwitchOn()
		{
			Enable();
		}

		public override void SwitchOff()
		{
			Disable();
		}
		
		#endregion
	}
}
