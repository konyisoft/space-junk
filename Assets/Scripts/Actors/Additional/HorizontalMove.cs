﻿using UnityEngine;

namespace Konyisoft
{
	public class HorizontalMove : GameBehaviour
	{
		#region Enumerations

		public enum Direction
		{
			Right,
			Left
		}
		
		#endregion
		
		#region Classes and structs
		
		[System.Serializable]
		public struct HorizontalConstraints
		{
			public float left;
			public float right;

			public HorizontalConstraints(float left, float right)
			{
				this.left = left;
				this.right = right;
			}
		}
		
		#endregion
		
		#region Fields and properties
		
		public HorizontalConstraints constraints;
		public Direction startDirection = Direction.Right;
		public float speed = 1f;
		public float waitTime = 0f;

		Direction currentDirection = Direction.Right;
		float waitingStartTime = 0f; 
		bool isWaiting = false;
		
		#endregion

		#region Mono methods
		
		protected override void Awake()
		{
			base.Awake();
			currentDirection = startDirection;
		}

		void Update()
		{
			// Waiting...
			if (isWaiting && Time.realtimeSinceStartup - waitingStartTime < waitTime)
				return;
			
			Vector3 targetPosition = GetTargetPosition();
			Transform.position = Vector3.MoveTowards(Transform.position, targetPosition, Time.deltaTime * speed);
			switch (currentDirection)
			{
				case Direction.Right:
				{
					if (Transform.position.x >= constraints.right)
					{
						// Begin waiting or reverse the direction
						if (waitTime > 0f && !isWaiting)
						{
							waitingStartTime = Time.realtimeSinceStartup;
							isWaiting = true;
						}
						else
						{
							currentDirection = Direction.Left;
							isWaiting = false;
						}
					}
				}
				break;

				case Direction.Left:
				{
					if (Transform.position.x <= constraints.left)
					{
						// Begin waiting or reverse the direction
						if (waitTime > 0f && !isWaiting)
						{
							waitingStartTime = Time.realtimeSinceStartup;
							isWaiting = true;
						}
						else
						{
							currentDirection = Direction.Right;
							isWaiting = false;
						}
					}
				}
				break;
			}
		}
		
		#endregion
		
		#region Private methods

		Vector3 GetTargetPosition()
		{
			return new Vector3(
                currentDirection == Direction.Right ? constraints.right : constraints.left,
                Transform.position.y,
                Transform.position.z
			);
		}
		
		#endregion
	}
}