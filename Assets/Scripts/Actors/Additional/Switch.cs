﻿using UnityEngine;

namespace Konyisoft
{
    public class Switch : GameBehaviour
    {
		#region Enumerations
		
		public enum State
		{
			On,
			Off
		}
		
		#endregion
		
		#region Fields and properties
		
		public Transform rigBase;
		public State startState = State.Off;
		public Switchable switchable;
		
		State currentState = State.Off;
		Quaternion targetRotation;
		bool isSwitching = false;
		
		#endregion
		
		#region Mono methods
		
		protected override void Awake()
        {
            base.Awake();
			
			// At start
			if (startState == State.On)
				On(true, false, false);
			else
				Off(true, false, false);
		}
		
		void Update()
		{
			if (rigBase != null && isSwitching)
			{
				 rigBase.rotation = Quaternion.Lerp(rigBase.rotation, targetRotation , Time.deltaTime * 10f);
				 float angle = Quaternion.Angle(rigBase.rotation, targetRotation);
				 if (angle < 1f)
				 {
					 rigBase.rotation = targetRotation;
					 isSwitching = false;
				 }
			}
		}
		
		#endregion		
		
		#region Private methods
		
		void On(bool immediate, bool playSound, bool callSwitchable)
		{
			if (rigBase != null && !isSwitching)
			{
				currentState = State.On;
				targetRotation = Quaternion.AngleAxis(25f, Vector3.forward);

				if (callSwitchable && switchable != null)
					switchable.SwitchOn();
				
				if (playSound)
					AudioManager.Instance.PlaySound("Switch");
				
				if (immediate)
				{
					rigBase.rotation = targetRotation;
				}
				else
				{
					isSwitching = true;
				}
			}
		}

		void Off(bool immediate, bool playSound, bool callSwitchable)
		{
			if (rigBase != null && !isSwitching)
			{
				currentState = State.Off;
				targetRotation = Quaternion.AngleAxis(-25f, Vector3.forward);				

				if (callSwitchable && switchable != null)
					switchable.SwitchOff();
				
				if (playSound)
					AudioManager.Instance.PlaySound("Switch");
				
				if (immediate)
				{
					rigBase.rotation = targetRotation;
				}
				else
				{
					isSwitching = true;
				}
			}
		}
		
		void Toggle()
		{
			if (currentState == State.On)
				Off(false, true, true);
			else
				On(false, true, true);
		}

		#endregion
		
		#region Event handlers
		
		void OnCollisionEnter(Collision collision)
		{
			// Hit by the ball?
			if (collision.gameObject.CompareTag(Constants.Tags.Ball))
			{
				Toggle();
			}
		}
		
		#endregion
	}
}
