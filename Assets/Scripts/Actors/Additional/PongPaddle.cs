﻿using UnityEngine;

namespace Konyisoft
{
	public class PongPaddle : GameBehaviour
	{
		#region Fields and properties
		
		public float paddleSpeed = 1f;
		public float minX = -3.5f;
		public float maxX = 3.5f;		
		
		#endregion

		#region Mono methods
		
		void Update()
		{
			// The AI, LOL
			if (GameManager.Instance.HasBall)
			{
				Ball ball = GameManager.Instance.GetClosestBall(Transform.position);
				Vector3 position = Transform.position;
				// If the ball is in the front of the paddle: follow its position
				if (position.z > ball.Transform.position.z)
				{
					position.x = Mathf.Clamp(ball.Transform.position.x, minX, maxX);
					Transform.position = Vector3.MoveTowards(Transform.position, position, Time.deltaTime * paddleSpeed);
				}
				// Move away from the ball (slowly)
				else
				{
					position.x = Mathf.Clamp(-ball.Transform.position.x, minX, maxX);
					Transform.position = Vector3.MoveTowards(Transform.position, position, Time.deltaTime * (paddleSpeed / 4f));					
				}
			}
		}

		void OnCollisionEnter(Collision collision)
		{
			if (collision.gameObject.CompareTag(Constants.Tags.Ball))
			{
				AudioManager.Instance.PlaySound("Pong Paddle Impact");
			}
		}
		
		#endregion
	}
}