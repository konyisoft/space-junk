﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Konyisoft
{
	public class TeleportContainer: GameBehaviour
	{
		#region Fields and properties
		
		public Teleport LastTarget
		{
			get; set;
		}
		
		List<Teleport> teleports;
		
		#endregion

		#region Mono methods
		
		protected override void Awake()
		{
			base.Awake();
			teleports = GetComponentsInChildren<Teleport>().ToList();
		}
		
		#endregion
		
		#region Public methods
		
		public Teleport GetRandomTeleport(Teleport excluded = null)
		{
			List<Teleport> teleports;
			if (excluded == null)
				teleports = this.teleports;
			else
				teleports = this.teleports.FindAll(teleport => teleport != excluded);
			
			return teleports.Count > 0 ?
				teleports[Random.Range(0, teleports.Count)] :
				null;
		}
		
		#endregion
	}
}