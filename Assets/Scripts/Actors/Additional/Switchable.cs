﻿namespace Konyisoft
{
    public abstract class Switchable : GameBehaviour
    {
		public abstract void SwitchOn();
		public abstract void SwitchOff();
	}
}
