﻿using UnityEngine;

namespace Konyisoft
{
	public class MovingPaddle : GameBehaviour
	{
		#region Mono methods
		
		void OnCollisionEnter(Collision collision)
		{
			if (collision.gameObject.CompareTag(Constants.Tags.Ball))
			{
				// Uses the pong's sfx
				AudioManager.Instance.PlaySound("Pong Paddle Impact");
			}
		}
		
		#endregion
	}
}