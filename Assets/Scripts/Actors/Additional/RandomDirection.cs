﻿using UnityEngine;

namespace Konyisoft
{
	public class RandomDirection: GameBehaviour
	{
		#region Mono methods
		
		void OnTriggerEnter(Collider collider)
		{
			// Hit by the ball?
			if (collider.gameObject.CompareTag(Constants.Tags.Ball))
			{
				GameManager.Instance.MainBall.RandomDirection();
			}
		}

		#endregion
	}
}