using UnityEngine;
using System.Collections;

namespace Konyisoft
{
	public class PowerUp : GameBehaviour
	{
		#region Fields and properties
		
		public float minSpeed = 4f;
		public float maxSpeed = 5f;
		public bool applyable = true;
		public PowerUpManager.PowerUpType type = PowerUpManager.PowerUpType.Unset;
		public float evaporateDuration = 0.5f;
		
		bool isDestroying = false;
		float speed = 5f;

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			PowerUpManager.Instance.RegisterPowerUp(this);
			speed = Random.Range(minSpeed, maxSpeed);
		}
		
		void Start()
		{
			AudioManager.Instance.PlaySound("PowerUp Start");
			Move();
		}
		
		void OnDestroy()
		{
			StopAllCoroutines();
			PowerUpManager.Instance.UnregisterPowerUp(this);
		}
		
		void OnTriggerEnter(Collider other)
		{
			if (!isDestroying)
			{
				if (other.gameObject.CompareTag(Constants.Tags.BackWall))
				{
					StartCoroutine(CO_Destroy());
				}
				else if (other.gameObject.CompareTag(Constants.Tags.Paddle))
				{
					GameManager.Instance.GetPowerUp(this);
				}
			}
		}
		
		#endregion
		
		#region Public methods

		public void Evaporate()
		{
			// When ball is out of field all powerups stop and destroy with a smoke effect
			if (!isDestroying)
			{
				Rigidbody.isKinematic = true;
				Rigidbody.velocity = Vector3.zero;
				Collider.enabled = false;
				EffectManager.Instance.InstantiateWhiteSmoke(Transform.position);
				StartCoroutine(CO_Evaporate());
			}
		}

		#endregion
		
		#region Private methods

		void Move()
		{
			Rigidbody.isKinematic = false;
			Vector3 direction = -Vector3.forward;  // Moves backward!
			Rigidbody.velocity = direction * speed;
		}
		
		IEnumerator CO_Destroy()
		{
			isDestroying = true;
			yield return new WaitForSeconds(1.5f);
			Destroy(gameObject);
		}

		IEnumerator CO_Evaporate()
		{
			isDestroying = true;
			float startTime = Time.time;
			Vector3 origScale = Transform.localScale;
			while (Time.time - startTime < evaporateDuration)
			{
				Transform.localScale = Vector3.Lerp(origScale, Vector3.zero, (Time.time - startTime) / evaporateDuration);
				yield return null;
			}
			yield return null;
			Destroy(gameObject);
		}
		
		#endregion
	}
}