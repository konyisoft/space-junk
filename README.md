# Space Junk
**Konyisoft, 2017**

Breakout game in space.  
Created with Unity3D 2017.4 on Ubuntu.  
Complete project.

[Space Junk on Google Play](https://play.google.com/store/apps/details?id=com.konyisoft.spacejunk)  
[Windows, Linux and HTML5 versions on GameJolt](https://gamejolt.com/games/space-junk/357691)
